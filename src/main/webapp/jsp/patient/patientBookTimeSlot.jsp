<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<rapid:override name="patientContext">
    <h1 class="mt-4 mb-4">Book time slot</h1>
    <c:forEach var="type" items="${errors}">
        <label class="text-danger">${type.value}</label><<br>
    </c:forEach>
    <form action="<c:url value="#"/>" method="post" class="probootstrap-form-appointment">
        <div class="form-group">
            <label for="doctorSelector">Select doctor:</label>
                <%--@elvariable id="doctorItem" type="com.salim.model.Doctor"--%>
            <div class="form-group">
                <select class="custom-select form-control" name="doctorUUID" id="doctorSelector">
                    <c:forEach items="${doctors}" var="doctorItem">
                        <option value="${doctorItem.uuid.toString()}" <c:if test="${param.get('doctorUUID1') == doctorItem.uuid.toString()}" > selected </c:if>>${doctorItem.user.firstName} ${doctorItem.user.lastName}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="probootstrap-date">Select date:</label>
                <span class="icon"><i class="icon-calendar"></i></span>
                <input type="text" name="date" id="probootstrap-date"
                       <% if (request.getParameter("date1")!= null){%>value="<%= request.getParameter("date1") %>"<%}%>
                       class="form-control BLACK-TEXT" autocomplete="off" required
                       placeholder="DD MM YYYY">
            </div>
            <div class="form-group">
                <label for="startTime">Start time: </label>
                <input type="text" class="timepicker form-control" name="startTime" id="startTime" required="required">
            </div>

            <div class="form-group">
                <label for="duration">Duration: </label>
                <input type="number" class="text-dark form-control" name="duration" <% if (request.getParameter("duration1")!= null){%>value="<%= request.getParameter("duration1") %>"<%}%>  id="duration" required="required">
            </div>


            <div class="form-group">
                <label for="nearest">Book nearest if time slot is busy? :</label>
                <input id="nearest" type="checkbox" name="nearest" class="check-box">
            </div>

        </div>
        <div class="form-group">
            <input type="submit" value="Book" class="btn btn-secondary">
        </div>
    </form>

</rapid:override>
<rapid:override name="src">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/wickedpicker.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/wickedpicker.js"></script>
    <script type="text/javascript">
        var options = {
            <% if (request.getParameter("startTime1")!= null){%> now: "<%= request.getParameter("startTime1") %>",<%}%>
            twentyFour: true,
            upArrow: 'wickedpicker__controls__control-up',
            downArrow: 'wickedpicker__controls__control-down',
            close: 'wickedpicker__close',
            hoverState: 'hover-state',
            title: 'Timepicker',
            showSeconds: false,
            minutesInterval: 1,
            beforeShow: null,
            show: null,
            clearable: false
        };
        $(".timepicker").wickedpicker(options);
    </script>
</rapid:override>
<%@ include file="../patient/patientWrapper.jsp" %>