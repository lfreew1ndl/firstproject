<%@ page import="com.salim.model.TimeSlot" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<rapid:override name="patientContext">
    <h1 class="mt-4 mb-4">Available time slots</h1>
    <form action="<c:url value="#"/>" method="post" class="probootstrap-form-appointment">
        <div class="form-group">
            <label for="doctorSelector">Select doctor:</label>
                <%--@elvariable id="doctorItem" type="com.salim.model.Doctor"--%>
            <div class="form-group">
                <select class="custom-select" name="doctorUUID" id="doctorSelector">
                    <c:forEach items="${doctors}" var="doctorItem">
                        <option value="${doctorItem.uuid.toString()}">${doctorItem.user.firstName} ${doctorItem.user.lastName}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group">
                <label for="probootstrap-date">Select date:</label>
                <span class="icon"><i class="icon-calendar"></i></span>
                <input type="text" name="date" id="probootstrap-date" class="form-control" autocomplete="off" required
                       placeholder="DD MM YYYY">
            </div>

        </div>
        <div class="form-group">
            <input type="submit" value="Search" class="btn btn-secondary">
        </div>
    </form>

    <% if (request.getAttribute("timeSlots") != null) {%>
    <% if (((List<TimeSlot>) request.getAttribute("timeSlots")).size() > 0) {%>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Time</th>
            <th>Duration</th>
            <th>Action</th>
        </tr>
        </thead>
            <%--@elvariable id="item" type="com.salim.model.TimeSlot"--%>
        <tbody>
        <c:forEach items="${timeSlots}" var="item">
            <tr>
                <td>${item.startTime}</td>
                <td>${item.duration}</td>
                <td><a class="text-warning"
                       href="<c:url value="/patient/bookTimeSlot?startTime1=${item.startTime}&duration1=${item.duration}&doctorUUID1=${doctorUUID1}&date1=${date1}"/>">Book</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%} else {%>
    <br>
    <label class="text-danger">There are no time slots for this date.</label>
    <%} %>
    <%} %>

</rapid:override>
<%@ include file="../patient/patientWrapper.jsp" %>