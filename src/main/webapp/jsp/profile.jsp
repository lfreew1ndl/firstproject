<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<rapid:override name="content">

    <section class="mb-5">
        <div class="container">
            <div>
                <div class="text-center" style="margin-left:20%; margin-right:20%">
                    <div>
                        <h1 class="display-4 text-center">Profile</h1>
                    </div>
                    <% if (request.getParameterValues("info") != null) {%>
                    <label class="text-success" id="info-error"><%= request.getParameterValues("info")[0] %>
                    </label>
                    <br>
                    <%}%>
                    <c:forEach var="type" items="${errors}">
                        <label class="text-danger">${type.value}</label><br>
                    </c:forEach>
                    <label class="text-center" style="font-size: x-large">First name: ${user.firstName}</label>
                    <br>
                    <label class="text-center" style="font-size: x-large">Last name: ${user.lastName}</label>
                    <br>
                    <label class="text-center" style="font-size: x-large">Email: ${user.email}</label>
                    <br>
                    <label class="text-center" style="font-size: x-large">Birthday: ${user.birthday}</label>
                    <br>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Edit
                    </button>
                    <%--<br>--%>
                    <%--<input type="submit" style="margin-top: 2px; margin-bottom: 2px" value="Change password" onclick=""--%>
                           <%--class="btn-warning btn text">--%>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" name="firstName" class="form-control BLACK-TEXT"
                                   placeholder="Your firstName" value="${user.firstName}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="lastName" value="${user.lastName}" class="form-control BLACK-TEXT"
                                   placeholder="Your lastName" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control BLACK-TEXT" value="${user.email}"
                                   placeholder="Your Email"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="icon"><i class="icon-calendar"></i></span>
                            <input type="text" name="date" id="probootstrap-date" autocomplete="off"
                                   class="form-control BLACK-TEXT" required value="${user.birthday}"
                                   placeholder="DD MM YYYY">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</rapid:override>
<rapid:override name="src">
    <script type="text/javascript">
        function delText(){
            var element = $("#info-error")[0];
            console.log(element);
            if (element !== undefined){
                element.setAttribute('style', 'display : none')
            }
        }

        setTimeout(delText, 5000);
    </script>
</rapid:override>



<%@ include file="wrapper.jsp" %>
