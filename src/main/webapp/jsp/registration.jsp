<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<rapid:override name="content">

  <section class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 mb-5">
          <h1 class="display-4">Registration</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="probootstrap-blog-appointment">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-md-6 pr-md-5 pr-0 pt-md-5 pt-0 pb-md-5 pb-0">
          <h2 class="h1 mb-4 text-white">Requirement</h2>
          <ul class="probootstrap-blog-list list-unstyled">
            <li>
              <h2>Login length must be between 8 and 20.</h2>
            </li>
            <li>
              <h2>Password length must be more then 8.</h2>
            </li>
            <li>
              <h2>All fields mustn't be empty.</h2>
            </li>
            <li>
              <h2>Passwords must match.</h2>
            </li>
          </ul>
        </div>
        <div class="col-md-6 p-md-5 p-3 probootstrap-aside-stretch-right">
          <h2 class="h1 text-white">Registration</h2>
            <c:forEach var="type" items="${errors}">
                <label class="text-danger">${type.value}</label><<br>
            </c:forEach>
          <form action="#" method="post" class="probootstrap-form-appointment">
            <div class="form-group">
              <input type="text" name="login" id="login" class="form-control WHITE-TEXT" placeholder="Your login" required>
            </div>
            <div class="form-group">
              <input type="text" name="firstName" class="form-control WHITE-TEXT" placeholder="Your firstName" required>
            </div>
            <div class="form-group">
              <input type="text" name="lastName"  class="form-control WHITE-TEXT" placeholder="Your lastName" required>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control WHITE-TEXT" placeholder="Your Email" required>
            </div>
            <div class="form-group">
              <input type="password" id="password" minlength="8" maxlength="20" name="password" class="form-control WHITE-TEXT" placeholder="Your password" required>
            </div>
            <div class="form-group">
              <input type="password" id="password2" minlength="8" maxlength="20" name="password2" class="form-control WHITE-TEXT" placeholder="Confirm Password" required>
              <label id="password-error" class="error"></label>
            </div>

            <div class="form-group">
              <span class="icon"><i class="icon-calendar"></i></span>
              <input type="text" name="date" id="probootstrap-date" autocomplete="off" class="form-control WHITE-TEXT" required
                     placeholder="DD MM YYYY">
            </div>

            <div class="form-group">
              <input type="submit" value="Register" onclick="validate()" class="btn btn-secondary">
            </div>

          </form>
        </div>
      </div>
    </div>
  </section>
  </rapid:override>
<rapid:override name="src">
  <script type="text/javascript">
     function validate() {
         var pass2 = $("#password2");
         if ($("#password")[0].value !== pass2[0].value){
             pass2[0].setCustomValidity("Passwords Don't Match!");
         }else {
             pass2[0].setCustomValidity('');
         }

         $.ajax({
             async: false,
             type: 'GET',
             url: window.location.origin+"/isUsernameAvailable?login="+$('#login')[0].value,
             success: function(resp) {
                 console.log(resp);
                 if (resp === "false"){
                     $("#login")[0].setCustomValidity("User with this login is already exist.");
                 }else{
                     $("#login")[0].setCustomValidity('');
                 }
             }
         });
    }

  </script>
</rapid:override>

<%@ include file="wrapper.jsp" %>