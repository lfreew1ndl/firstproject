<%@ page import="com.salim.model.Visit" %>
<%@ page import="java.util.List" %>
<%@ page import="com.salim.model.Doctor" %>
<%@ page import="com.salim.model.Work" %>
<%@ page import="java.util.Optional" %>
<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<rapid:override name="doctorContext">
    <h1 class="mt-4 mb-4">Visits</h1>
    <% if (request.getParameterValues("info") != null) {%>
    <label class="text-success" id="info-error"><%= request.getParameterValues("info")[0] %>
    </label>
    <%} else if (request.getParameterValues("error") != null) {%>
    <label class="text-danger" id="info-error"><%= request.getParameterValues("error")[0] %>
    </label>
    <%}%>
    <%Work work = (((Doctor) request.getAttribute("doctor")).getWorkList().stream().filter(e -> e.getVisits().size() > 0)).findFirst().orElse(null);
    if (work!= null){%>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Patient</th>
            <th>Date</th>
            <th>Time</th>
            <th>Duration</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
            <%--@elvariable id="doctor" type="com.salim.model.Doctor"--%>
            <%--@elvariable id="item" type="com.salim.model.Work"--%>
            <%--@elvariable id="visit" type="com.salim.model.Visit"--%>
        <tbody>
            <c:forEach items="${visits}" var="visit">
                <tr <c:if test="${visit.status == 'cancel'}"> style="background-color: rgba(253, 183, 179, 0.49)" </c:if>
                        <c:if test="${visit.status == 'successful'}"> style="background-color: rgba(152, 251, 152, 0.49)" </c:if>>
                    <td>${visit.user.firstName} ${visit.user.lastName}</td>
                    <td>${visit.work.date}</td>
                    <td>${visit.timeSlot.startTime}</td>
                    <td>${visit.timeSlot.duration}</td>
                    <td>${visit.status}</td>
                    <td>
                        <c:if test="${visit.status == 'wait'}">
                            <a href="#deleteEmployeeModal" data-id="${visit.uuid}" class="btn-danger delete-record"
                               data-toggle="modal"><i class="icon-circle-with-cross" data-toggle="tooltip"
                                                      title="Delete"></i></a>
                            <a href="<c:url value="/recordDone?record_id=${visit.uuid}"/>" class="btn-success"><i class="icon-check" data-toggle="tooltip"
                                                      title="Done"></i></a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <% }else{ %>
    <br>
    <label class="text-danger">You don't have visits yet.</label>
    <% }%>
    <div id="deleteEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#" method="post" onsubmit="return validate()">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete visit</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning">
                            <small>This action cannot be undone.</small>
                        </p>
                        <input id="bookId" name="record_id" type="text" style="display: none">
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>
</rapid:override>

    <rapid:override name="src">
        <script type="text/javascript">
            $(document).on("click", ".delete-record", function () {
                var myBookId = $(this).data('id');
                $(".modal-body #bookId").val(myBookId);
            });

            function delText() {
                var element = $("#info-error")[0];
                console.log(element);
                if (element !== undefined) {
                    element.setAttribute('style', 'display : none')
                }
            }

            setTimeout(delText, 5000);
        </script>
    </rapid:override>
<%@ include file="../doctor/doctorWrapper.jsp" %>