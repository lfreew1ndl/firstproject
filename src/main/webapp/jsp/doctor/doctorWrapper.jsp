<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<rapid:override name="content">

    <section class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h1 class="display-4">Doctor</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-services">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-md-3 pb-5 probootstrap-aside-stretch-left probootstrap-inside">
                    <div class="mb-3 pt-5">
                        <h2 class="h6">Menu</h2>
                        <ul class="list-unstyled probootstrap-light mb-4">
                            <li><a href="<c:url value="/doctor"/>">Visits</a></li>
                            <li><a href="<c:url value="/doctor/work"/>">All work days</a></li>
                            <li><a href="<c:url value="/doctor/addWorkDay"/>">Add work day</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 pl-md-5 pb-5 pl-0 probootstrap-inside">
                    <rapid:block name="doctorContext">
                        base_body_content
                    </rapid:block>
                </div>
            </div>
        </div>
    </section>
</rapid:override>

<%@ include file="../wrapper.jsp" %>