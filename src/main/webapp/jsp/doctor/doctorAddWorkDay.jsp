<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<rapid:override name="doctorContext">


    <h1 class="mt-4 mb-4">Add work day</h1>
    <% if (request.getParameterValues("info") != null) {%>
    <label class="text-success" id="info-error"><%= request.getParameterValues("info")[0] %>
    </label>
    <%}else if (request.getParameterValues("error") != null) {%>
    <label class="text-danger" id="info-error"><%= request.getParameterValues("error")[0] %>
    </label>
    <%}%>
    <c:forEach var="type" items="${errors}">
        <label class="text-danger">${type.value}</label><br>
    </c:forEach>
    <form action="<c:url value="#"/>" method="post" class="probootstrap-form-appointment">
        <div class="form-group">

            <div class="form-group">
                <label for="probootstrap-date">Date: </label>
                <span class="icon"><i class="icon-calendar"></i></span>
                <input type="text" name="date" id="probootstrap-date" autocomplete="off" class="form-control BLACK-TEXT" required
                       placeholder="DD MM YYYY">
            </div>
            <br>

            <div>
                <label for="startTime">Start work time: </label>
                <input type="text" class="timepicker" name="startTime" id="startTime" required="required">
            </div>

            <div>
                <label for="endTime">End work time: </label>
                <input type="text" class="timepicker" name="endTime" id="endTime" required="required">
            </div>


        </div>
        <div class="form-group">
            <input type="submit" value="Add" class="btn btn-secondary">
        </div>
    </form>

</rapid:override>

<rapid:override name="src">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/wickedpicker.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/wickedpicker.js"></script>
    <script type="text/javascript">
        var options = {
            twentyFour: true,
            upArrow: 'wickedpicker__controls__control-up',
            downArrow: 'wickedpicker__controls__control-down',
            close: 'wickedpicker__close',
            hoverState: 'hover-state',
            title: 'Timepicker',
            showSeconds: false,
            minutesInterval: 1,
            beforeShow: null,
            show: null,
            clearable: false
        };
        $(".timepicker").wickedpicker(options);
    </script>
</rapid:override>
<%@ include file="../doctor/doctorWrapper.jsp" %>