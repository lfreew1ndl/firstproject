<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<rapid:override name="content">

    <section class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h1 class="display-4">About Us</h1>
                </div>
            </div>
        </div>
    </section>



    <section class="probootstrap-services">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-md-9 pl-md-5 pb-5 pl-0 probootstrap-inside">
                    <h1 class="mt-4 mb-4">Overview</h1>
<p>Stanford Health Care-Stanford Hospital in Stanford, Calif. is ranked No. 9 on the Best Hospitals Honor Roll. It is nationally ranked in 12 adult specialties and rated high performing in 2 adult specialties and 9 procedures and conditions. It is a general medical and surgical facility. It is a teaching hospital. Stanford Hospital was founded in 1959, when the university’s medical school moved from San Francisco to the campus in Palo Alto. The Stanford Medical Center’s main campus includes the Lucile Packard Children’s Hospital, which opened in 1991, and the Stanford Advanced Medical Center, which is the cancer treatment facility. Stanford Health Care is building a new main hospital that is scheduled to open in 2019. Stanford’s cardiology department invented a treatment for atrial fibrillation that doesn’t destroy a lot of heart tissue. Pediatric cardiologists at the children’s hospital are using virtual reality technology in order to help explain congenital heart defects to patients and families. At the Stanford Cancer Center, genetic counseling can help families understand hereditary risks for cancer. Beyond medical care, cancer patients at Stanford can take classes that educate them on treatment and access activities such as mindfulness meditation, knitting and cosmetic techniques to counteract some effects of treatment. The Stanford Ear Institute, which includes a Children’s Hearing Center at Lucile Packard Children’s Hospital, performs microsurgery and installs cochlear implants. Among the specialties in Stanford’s Gynecology Division are minimally invasive and laparoscopic/robotic surgery. Among innovations at Stanford were the first adult heart transplant in the U.S., the world’s first successful heart-lung transplant and the discovery that exercise increases “good” cholesterol.</p>
                </div>
            </div>
        </div>
    </section>
</rapid:override>


<%@ include file="wrapper.jsp" %>
