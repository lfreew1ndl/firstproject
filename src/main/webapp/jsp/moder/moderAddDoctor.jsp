<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<rapid:override name="moderContext">
    <div class="col-md-9 pl-md-5 pb-5 pl-0 probootstrap-inside">
        <h1 class="mt-4 mb-4">Add doctor</h1>

        <form action="<c:url value="#"/>" method="post" class="probootstrap-form-appointment">
            <div class="form-group">
                <label for="userSelector">Select user:</label>
                    <%--@elvariable id="doctorItem" type="com.salim.model.User"--%>
                <div class="form-group">
                    <select class="custom-select" name="userUUID" id="userSelector" required>
                        <c:forEach items="${users}" var="userItem">
                            <option value="${userItem.uuid.toString()}">${userItem.firstName} ${userItem.lastName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="profession" class="text-dark" style="border-radius: 5px"
                           placeholder="Profession">
                </div>

            </div>
            <div class="form-group">
                <input type="submit" value="Add" class="btn btn-secondary">
            </div>
        </form>
    </div>
</rapid:override>

<%@ include file="../moder/moderWrapper.jsp" %>