<%@ page import="com.salim.model.User" %>
<%@ page import="com.salim.model.enums.Roles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health - Free HTML5 Bootstrap 4 Template by uicookies.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Free HTML5 Website Template by uicookies.com" />
  <meta name="keywords" content="free bootstrap 4, free bootstrap 4 template, free website templates, free html5, free template, free website template, html5, css3, mobile first, responsive" />
  <meta name="author" content="uicookies.com" />

  <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/open-iconic-bootstrap.min.css">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/owl.carousel.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/owl.theme.default.min.css">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/icomoon.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/flaticon.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark probootstrap-navbar-dark">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-nav" aria-controls="probootstrap-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="probootstrap-nav">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item"><a href="<c:url value="/"/>" class="nav-link pl-0">Home</a></li>
        <li class="nav-item"><a href="<c:url value="/about"/>" class="nav-link">About</a></li>
        <% User user = (User) session.getAttribute("user");
        if (user != null && user.getRoles().contains(Roles.user)){%>
        <li class="nav-item"><a href="<c:url value="/patient"/>" class="nav-link">Patient</a></li>
        <%}%>
        <%if (user != null && user.getRoles().contains(Roles.doctor)){%>
        <li class="nav-item"><a href="<c:url value="/doctor"/>" class="nav-link">Doctor</a></li>
        <%}%>
        <%if (user != null && user.getRoles().contains(Roles.admin)){%>
        <li class="nav-item"><a href="<c:url value="/staff"/>" class="nav-link">Moder</a></li>
        <%}%>



      </ul>
      <div class="ml-auto">
        <ul class="navbar-nav mr-auto">
            <% if (session.getAttribute("user") == null ) {%>
          <li class="nav-item"><a href="<c:url value="/login"/>" class="nav-link pl-0">Sign in</a></li>
          <li class="nav-item"><a href="<c:url value="/register"/>" class="nav-link">Sign up</a></li>
            <%}else{%>
            <li class="nav-item"><a href="<c:url value="/profile"/>" class="nav-link">Profile</a></li>
            <li class="nav-item"><a href="<c:url value="/logout"/>" class="nav-link">Log out</a></li>
            <%}%>
        </ul>
      </div>
    </div>
  </div>
</nav>
<!-- END nav -->
<header role="banner" class="probootstrap-header py-5">
  <div class="container">
    <div class="row">
      <div class="col-md-3 mb-4">
        <a href="<c:url value="/"/>" class="mr-auto"><img src="${pageContext.request.contextPath}/images/logo.png" width="212" height="48" class="hires" alt="Free Template by uiCookies"></a>
      </div>
      <div class="col-md-9">
        <div class="float-md-right float-none">
          <div class="probootstrap-contact-phone d-flex align-items-top mb-3 float-left">
            <span class="icon mr-2"><i class="icon-phone"></i></span>
            <span class="probootstrap-text"> +380964967270 <small class="d-block"><a href="#" class="arrow-link">Appointment <i class="icon-chevron-right"></i></a></small></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<rapid:block name="content">
  base_body_content
</rapid:block>

<footer class="probootstrap-footer">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-3">
        <h3 class="heading">Head Office</h3>
        <p class="mb-5">98 West 21th Street, Suite 721 New York NY 10016</p>
      </div>
      <div class="col-md-3">
        <h3 class="heading text-white">Open</h3>
        <p>
          Mon-Fri 7:30-18:00 <br>
          Sat 7:30-18:00 <br>
          Sun 7:30-18:00
        </p>
      </div>
      <div class="col-md-3">
        <h3 class="heading">Quick Links</h3>
        <ul class="list-unstyled probootstrap-footer-links">
          <li><a href="<c:url value="/"/>">Home</a></li>
          <li><a href="<c:url value="/about"/>">About</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h3 class="heading">Follow us</h3>
        <ul class="probootstrap-footer-social">
          <li><a href="#"><span class="icon-twitter"></span></a></li>
          <li><a href="#"><span class="icon-facebook"></span></a></li>
          <li><a href="#"><span class="icon-linkedin"></span></a></li>
        </ul>
      </div>
    </div>
    <!-- END row -->
    <div class="row probootstrap-copyright">
      <div class="col-md-12">
        <p><small>&copy; 2017 <a href="https://uicookies.com/" target="_blank">uiCookies Health</a>. All Rights Reserved. Designed &amp; Developed by <a href="https://uicookies.com/" target="_blank">uicookies.com</a> (please don't remove credit, see <a href="https://uicookies.com/license/" target="_blank">license</a>)  <br> Demo Images <a href="https://pexels.com/">Pexels</a> </small></p>
      </div>
    </div>
  </div>
</footer>

<!-- loader -->
<div id="probootstrap-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#32609e"/></svg></div>


<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.waypoints.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.animateNumber.min.js"></script>
<rapid:block name="src">
</rapid:block>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

</body>
</html>