<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<rapid:override name="content"><%--@elvariable id="user" type="com.salim.model.User"--%>

    <section class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h1 class="display-4">The Future Of Medical Website</h1>
                </div>
            </div>
        </div>
    </section>


    <section class="probootstrap-section overlay bg-image" style="background-image: url(../images/bg_1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="text-white display-4">Specialists in Family Healthcare</h2>
                    <p class="text-white mb-5 lead">Far far away, behind the word mountains, far from the countries
                        Vokalia.</p>
                    <div class="row justify-content-center mb-5">
                        <div class="col-md-4"><a href="#" class="btn btn-secondary btn-block">Appointment <span
                                class="icon-arrow-right"></span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="media d-block mb-5 text-center probootstrap-media">
                        <div class="probootstrap-icon mb-3"><span class="flaticon-price-tag display-4"></span></div>
                        <div class="media-body">
                            <h3 class="h5 mt-0 text-secondary">Medical Pricing</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media d-block mb-5 text-center probootstrap-media">
                        <div class="probootstrap-icon mb-3"><span class="flaticon-shield-with-cross display-4"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 mt-0 text-secondary">Quality &amp; Safety</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media d-block mb-5 text-center probootstrap-media">
                        <div class="probootstrap-icon mb-3"><span class="flaticon-microscope display-4"></span></div>
                        <div class="media-body">
                            <h3 class="h5 mt-0 text-secondary">Cutting-Edge Equipment</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="media d-block mb-5 text-center probootstrap-media">
                        <div class="probootstrap-icon mb-3"><span class="flaticon-microscope display-4"></span></div>
                        <div class="media-body">
                            <h3 class="h5 mt-0 text-secondary">Cutting-Edge Equipment</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media d-block mb-5 text-center probootstrap-media">
                        <div class="probootstrap-icon mb-3"><span
                                class="flaticon-gym-control-of-exercises-with-a-list-on-a-clipboard-and-heart-beats display-4"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 mt-0 text-secondary">Personalized Treatment</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media d-block mb-5 text-center probootstrap-media">
                        <div class="probootstrap-icon mb-3"><span class="flaticon-doctor display-4"></span></div>
                        <div class="media-body">
                            <h3 class="h5 mt-0 text-secondary">Experience Physicians</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
                        </div>
                    </div>
                </div>

            </div>
            <!--  <div class="row">

            </div> -->
        </div>
    </section>


    <section class="probootstrap-section" id="section-counter">
        <div class="container">
            <div class="row">
                <div class="col-md probootstrap-animate">
                    <div class="probootstrap-counter text-center">
                        <span class="probootstrap-number" data-number="1">0</span>
                        <span class="probootstrap-label">Founders</span>
                    </div>
                </div>
                <div class="col-md probootstrap-animate">
                    <div class="probootstrap-counter text-center">
                        <span class="probootstrap-number" data-number="${doctors}">0</span>
                        <span class="probootstrap-label">Number of Staffs</span>
                    </div>
                </div>
                <div class="col-md probootstrap-animate">
                    <div class="probootstrap-counter text-center">
                        <span class="probootstrap-number" data-number="${patients}">0</span>
                        <span class="probootstrap-label">Happy Patients</span>
                    </div>
                </div>
                <div class="col-md probootstrap-animate">
                    <div class="probootstrap-counter text-center">
                        <span class="probootstrap-number" data-number="10">0</span>
                        <span class="probootstrap-label">Business Partner</span>
                    </div>
                </div>
            </div>
        </div>

    </section>
</rapid:override>


<%@ include file="wrapper.jsp" %>