<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<rapid:override name="content">

    <section class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h1 class="display-4"> Permission denied.</h1>
                    <label style="font-size: x-large">You don't have permission to access this page.</label>
                </div>
            </div>
        </div>
    </section>

</rapid:override>


<%@ include file="wrapper.jsp" %>
