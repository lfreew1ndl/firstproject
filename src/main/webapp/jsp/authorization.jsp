<%@ taglib prefix="rapid" uri="http://www.rapid-framework.org.cn/rapid" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<rapid:override name="content">

  <section class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 mb-5">
          <h1 class="display-4">Login</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="probootstrap-blog-appointment">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-md-6 pr-md-5 pr-0 pt-md-5 pt-0 pb-md-5 pb-0">
          <h2 class="h1 mb-4 text-white">Instructions</h2>
          <ul class="probootstrap-blog-list list-unstyled">
            <li>
              <h2>Input your login </h2>
            </li>
            <li>
              <h2>Input your password</h2>
            </li>
          </ul>
        </div>
        <div class="col-md-6 p-md-5 p-3 probootstrap-aside-stretch-right">
          <h2 class="h1 text-white">Authorization</h2>
          <% if (request.getAttribute("errorMessage") != null) {%>
          <label class="text-danger"><%=request.getAttribute("errorMessage") %></label>
          <% } %>
          <form action="#" method="post" class="probootstrap-form-appointment">
            <div class="form-group">
              <input type="text" name="login" class="form-control WHITE-TEXT" placeholder="Your login" required>
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control WHITE-TEXT" placeholder="Your password" required>
            </div>
            <div class="form-group">
              <input type="submit" value="Log in" class="btn btn-secondary">
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>


  </rapid:override>

<%@ include file="wrapper.jsp" %>