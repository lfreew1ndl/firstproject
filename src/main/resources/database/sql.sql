
SET client_encoding = 'UTF8';

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public;



CREATE FUNCTION infanticide() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM time_slot WHERE time_slot.id = old.time_slot_id;
  RETURN NULL;
END; $$;


ALTER FUNCTION public.infanticide() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 188 (class 1259 OID 93344)
-- Name: doctor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE doctor (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    profession character(30) NOT NULL,
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE doctor OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 93349)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE role (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    role character varying(30) NOT NULL
);


ALTER TABLE role OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 93353)
-- Name: time_slot; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE time_slot (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    start_time time without time zone NOT NULL,
    duration integer NOT NULL
);


ALTER TABLE time_slot OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 93357)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    login character varying(30) NOT NULL,
    password text NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    birthday date,
    email character varying(30) NOT NULL
);


ALTER TABLE "user" OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 93364)
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_role (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    role_id uuid NOT NULL
);


ALTER TABLE user_role OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 93463)
-- Name: visit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE visit (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    time_slot_id uuid NOT NULL,
    work_id uuid NOT NULL,
    status text DEFAULT 'wait'::text NOT NULL
);


ALTER TABLE visit OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 93376)
-- Name: work; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE work (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    date date NOT NULL,
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL,
    doctor_id uuid,
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE work OWNER TO postgres;

--
-- TOC entry 2220 (class 0 OID 93344)
-- Dependencies: 188
-- Data for Name: doctor; Type: TABLE DATA; Schema: public; Owner: postgres
--
-- TOC entry 2076 (class 2606 OID 93381)
-- Name: doctor Doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY doctor
    ADD CONSTRAINT "Doctor_pkey" PRIMARY KEY (id);


--
-- TOC entry 2083 (class 2606 OID 93383)
-- Name: time_slot TimeSlot_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY time_slot
    ADD CONSTRAINT "TimeSlot_pkey" PRIMARY KEY (id);


--
-- TOC entry 2095 (class 2606 OID 93472)
-- Name: visit Visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visit
    ADD CONSTRAINT "Visit_pkey" PRIMARY KEY (id);


--
-- TOC entry 2080 (class 2606 OID 93387)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2085 (class 2606 OID 93389)
-- Name: user user_id_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_id_pk PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 93391)
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);


--
-- TOC entry 2093 (class 2606 OID 93393)
-- Name: work work_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY work
    ADD CONSTRAINT work_pkey PRIMARY KEY (id);


--
-- TOC entry 2077 (class 1259 OID 93394)
-- Name: doctor_user_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX doctor_user_id_uindex ON doctor USING btree (user_id);


--
-- TOC entry 2078 (class 1259 OID 93395)
-- Name: role_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX role_id_uindex ON role USING btree (id);


--
-- TOC entry 2081 (class 1259 OID 93396)
-- Name: role_role_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX role_role_uindex ON role USING btree (role);


--
-- TOC entry 2086 (class 1259 OID 93397)
-- Name: user_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_id_uindex ON "user" USING btree (id);


--
-- TOC entry 2087 (class 1259 OID 93398)
-- Name: user_login_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_login_uindex ON "user" USING btree (login);


--
-- TOC entry 2088 (class 1259 OID 93399)
-- Name: user_role_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_role_id_uindex ON user_role USING btree (id);


--
-- TOC entry 2091 (class 1259 OID 93400)
-- Name: work_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX work_id_uindex ON work USING btree (id);


--
-- TOC entry 2102 (class 2620 OID 93473)
-- Name: visit dead_kids; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER dead_kids AFTER DELETE ON visit FOR EACH ROW EXECUTE PROCEDURE infanticide();


--
-- TOC entry 2097 (class 2606 OID 93402)
-- Name: work doctor_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY work
    ADD CONSTRAINT doctor_id FOREIGN KEY (doctor_id) REFERENCES doctor(id);


--
-- TOC entry 2096 (class 2606 OID 93407)
-- Name: doctor doctor_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY doctor
    ADD CONSTRAINT doctor_user_id_fk FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- TOC entry 2098 (class 2606 OID 93474)
-- Name: visit time_slot_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visit
    ADD CONSTRAINT time_slot_id FOREIGN KEY (time_slot_id) REFERENCES time_slot(id);


--
-- TOC entry 2099 (class 2606 OID 93479)
-- Name: visit user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visit
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- TOC entry 2100 (class 2606 OID 93484)
-- Name: visit visit_time_slot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visit
    ADD CONSTRAINT visit_time_slot_id_fkey FOREIGN KEY (time_slot_id) REFERENCES time_slot(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2101 (class 2606 OID 93489)
-- Name: visit visit_work_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visit
    ADD CONSTRAINT visit_work_id_fk FOREIGN KEY (work_id) REFERENCES work(id);


-- Completed on 2018-11-20 15:14:19

--
-- PostgreSQL database dump complete
--

insert into role (id, role) VALUES ('a649b0b2-4493-4179-8ef6-b35a5073f3ec','user');
insert into role (id, role) VALUES ('6cefc11d-2bd5-4284-8e07-a9c9b988078b','doctor');
insert into role (id, role) VALUES ('9d26c319-792b-4833-8930-2b77f284ae74','admin');

insert into "user" (id, login, password, first_name, last_name, birthday, email) values ('22ca2a1e-7bd1-4577-a4fb-d42d2393c7a1','durex77','$2a$15$qO5Fik4KuWpII1DHgFAtGuHq6QwfjB3fHIKPo0h2YpiuUwXPno91W','Salim','Abdul','1998-12-30','salimabdul.it@gmail.com');

insert into "user" (id, login, password, first_name, last_name, birthday, email) values ('22ca2a1e-7bd1-4577-a4fb-d42d2393c7a2','doctor1','$2a$15$qO5Fik4KuWpII1DHgFAtGuHq6QwfjB3fHIKPo0h2YpiuUwXPno91W','Gregory','House','1998-12-30','salimabdul.it@gmail.com');
insert into "user" (id, login, password, first_name, last_name, birthday, email) values ('22ca2a1e-7bd1-4577-a4fb-d42d2393c7a3','doctor2','$2a$15$qO5Fik4KuWpII1DHgFAtGuHq6QwfjB3fHIKPo0h2YpiuUwXPno91W','Doctor','Death','1998-12-30','salimabdul.it@gmail.com');

insert into doctor (id, user_id, profession, active) VALUES ('9d26c319-792b-4833-8930-2b77f284ae55','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a2','Pediatrician',true);
insert into doctor (id, user_id, profession, active) VALUES ('9d26c319-792b-4833-8930-2b77f284ae56','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a3','Surgeon',true);

insert into "user" (id, login, password, first_name, last_name, birthday, email) values ('22ca2a1e-7bd1-4577-a4fb-d42d2393c7a4','user1','$2a$15$qO5Fik4KuWpII1DHgFAtGuHq6QwfjB3fHIKPo0h2YpiuUwXPno91W','Petro','Babuk','1998-12-30','salimabdul.it@gmail.com');
insert into "user" (id, login, password, first_name, last_name, birthday, email) values ('22ca2a1e-7bd1-4577-a4fb-d42d2393c7a5','user2','$2a$15$qO5Fik4KuWpII1DHgFAtGuHq6QwfjB3fHIKPo0h2YpiuUwXPno91W','Vasya','Babuk','1998-12-30','salimabdul.it@gmail.com');
insert into "user" (id, login, password, first_name, last_name, birthday, email) values ('22ca2a1e-7bd1-4577-a4fb-d42d2393c7a6','user3','$2a$15$qO5Fik4KuWpII1DHgFAtGuHq6QwfjB3fHIKPo0h2YpiuUwXPno91W','Nastya','Kaprosh','1998-12-30','salimabdul.it@gmail.com');

insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153893','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a1','a649b0b2-4493-4179-8ef6-b35a5073f3ec');
insert into user_role (id, user_id, role_id) VALUES ('4bb0671c-2805-43d7-aed3-c4e3aa770733','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a1','9d26c319-792b-4833-8930-2b77f284ae74');

insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153894','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a2','a649b0b2-4493-4179-8ef6-b35a5073f3ec');
insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153895','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a3','a649b0b2-4493-4179-8ef6-b35a5073f3ec');
insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153896','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a4','a649b0b2-4493-4179-8ef6-b35a5073f3ec');
insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153897','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a5','a649b0b2-4493-4179-8ef6-b35a5073f3ec');
insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153898','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a6','a649b0b2-4493-4179-8ef6-b35a5073f3ec');

insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153899','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a2','6cefc11d-2bd5-4284-8e07-a9c9b988078b');
insert into user_role (id, user_id, role_id) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153800','22ca2a1e-7bd1-4577-a4fb-d42d2393c7a3','6cefc11d-2bd5-4284-8e07-a9c9b988078b');

insert into work (id, date, start_time, end_time, doctor_id, active) VALUES ('6545e1e4-a5fa-44c5-aef0-a58ab0153800','2018-11-23','8:00','20:00','9d26c319-792b-4833-8930-2b77f284ae55',true );
insert into work (id, date, start_time, end_time, doctor_id, active) VALUES ('6545e1e5-a5fa-44c5-aef0-a58ab0153800','2018-11-23','8:00','20:00','9d26c319-792b-4833-8930-2b77f284ae56',true );
insert into work (id, date, start_time, end_time, doctor_id, active) VALUES ('6545e1e6-a5fa-44c5-aef0-a58ab0153800','2018-11-24','8:00','20:00','9d26c319-792b-4833-8930-2b77f284ae55',true );
insert into work (id, date, start_time, end_time, doctor_id, active) VALUES ('6545e1e7-a5fa-44c5-aef0-a58ab0153800','2018-11-24','8:00','20:00','9d26c319-792b-4833-8930-2b77f284ae56',true );
insert into work (id, date, start_time, end_time, doctor_id, active) VALUES ('6545e1e8-a5fa-44c5-aef0-a58ab0153800','2018-11-25','8:00','20:00','9d26c319-792b-4833-8930-2b77f284ae55',true );
insert into work (id, date, start_time, end_time, doctor_id, active) VALUES ('6545e1e9-a5fa-44c5-aef0-a58ab0153800','2018-11-25','8:00','20:00','9d26c319-792b-4833-8930-2b77f284ae56',true );
