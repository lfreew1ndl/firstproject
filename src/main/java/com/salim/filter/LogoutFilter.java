package com.salim.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/logout","/profile"})
public class LogoutFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login";
        String permissionURI = request.getContextPath() + "/permissionURI";

        boolean loggedIn = session != null && session.getAttribute("user") != null;
        if (loggedIn)
            chain.doFilter(request, response);
        else
            response.sendRedirect(loginURI);
    }

    @Override
    public void destroy() {

    }
}