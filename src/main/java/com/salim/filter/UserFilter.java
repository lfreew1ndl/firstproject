package com.salim.filter;

import com.salim.model.User;
import com.salim.model.enums.Roles;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/patient/*"})
public class UserFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login";
        String permissionURI = request.getContextPath() + "/permissionURI";

        boolean notLoggedIn = session != null && session.getAttribute("user") != null;
        if (notLoggedIn) {
            User user = (User) session.getAttribute("user");
            if (user.getRoles().contains(Roles.user))
                chain.doFilter(request, response);
            else
                response.sendRedirect(permissionURI);
        }else
            response.sendRedirect(loginURI);
    }

    @Override
    public void destroy() {

    }
}