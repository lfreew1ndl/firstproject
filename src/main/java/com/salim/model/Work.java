package com.salim.model;

import com.salim.model.exception.NoSuchTimeSlotAvailableException;
import com.salim.model.exception.TimeSlotBusyException;
import com.salim.service.VisitService;
import com.salim.validator.WorkDate;


import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created by Salim Abdul on 29.10.2018.
 */

@com.salim.validator.Work
public class Work {
    private UUID uuid;
    @NotNull(message = "Start time can't be empty")
    private LocalTime startTime;
    @NotNull(message = "End time can't be empty")
    private LocalTime endTime;
    @WorkDate
    @NotNull(message = "Date can't be empty")
    private LocalDate date;
    private Set<Visit> visits = new TreeSet<>((o1, o2) -> {
        if (o1.getStartTime().equals(o2.getStartTime()))
            return o1.getEndTime().compareTo(o2.getEndTime());
        return o1.getStartTime().compareTo(o2.getStartTime());
    });
    private UUID doctor_id;
    private Doctor doctor;
    private boolean isActive;
    private VisitService visitService = new VisitService();
    public Work() {
    }
    
    public void bookTimeSlot(TimeSlot timeSlot, User user) {
        if (isTimeSlotAvailable(timeSlot)) {
            visits.add(new Visit().setUser(user).setTimeSlot(timeSlot));
        }else
            throw new TimeSlotBusyException();

    }

    public boolean isActive() {
        return isActive;
    }

    public Work setActive(boolean active) {
        isActive = active;
        return this;
    }

    @SuppressWarnings("Duplicates")
    public TimeSlot getNearestTimeSlot(TimeSlot timeSlot) throws NoSuchTimeSlotAvailableException {
        Set<Visit> visits1 = visits.stream().filter(e -> !e.getStatus().equals("cancel")).collect(Collectors.toSet());

        if (timeSlot.getDuration() == 0)
            throw new NoSuchTimeSlotAvailableException("Not available slots with this duration");
        if (visits1.size() == 0 && Duration.between(startTime, endTime).toMinutes() > timeSlot.getDuration())
            if (timeSlot.getStartTime().isBefore(startTime))
                return new TimeSlot().setStartTime(startTime).setDuration(timeSlot.getDuration());
            else if (timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isAfter(endTime))
                return new TimeSlot().setStartTime(endTime.minusMinutes(timeSlot.getDuration())).setDuration(timeSlot.getDuration());
            else
                return timeSlot;
        if (timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isBefore(timeSlot.getStartTime())) {
            timeSlot.setStartTime(endTime.minusMinutes(timeSlot.getDuration()));
        }
        List<LocalTime> tempList = new ArrayList<>();
        Iterator<Visit> iterator = visits1.iterator();
        LocalTime tempStartTime = startTime;
        Visit next = null;
        while (iterator.hasNext()){
            next = iterator.next();
            LocalTime tempEndTime = next.getStartTime();
            addTimeSlotToList(timeSlot, tempList, tempStartTime, tempEndTime);
            tempStartTime = next.getEndTime();
        }
        if (next!=null){
            addTimeSlotToList(timeSlot, tempList, tempStartTime, endTime);
        }

        LocalTime localTime = tempList.stream().min((o2, o1) -> (int) (Math.abs(Duration.between(o2, timeSlot.getStartTime()).toMinutes()) - Math.abs(Duration.between(o1, timeSlot.getStartTime()).toMinutes()))).orElseThrow(() -> new NoSuchTimeSlotAvailableException("Not available slots with this duration"));
        return new TimeSlot().setStartTime(localTime).setDuration(timeSlot.getDuration());
    }

    @SuppressWarnings("Duplicates")
    public TimeSlot getNearestTimeSlotToUser(TimeSlot timeSlot,User user) throws NoSuchTimeSlotAvailableException, SQLException, ClassNotFoundException {
        Set<Visit> visits1 = new TreeSet<>();
        visits1.addAll(visits);
        visits1.addAll(visitService.findByUser(user));
        visits1 = visits1.stream().filter(e -> !e.getStatus().equals("cancel")).collect(Collectors.toSet());
        if (timeSlot.getDuration() == 0)
            throw new NoSuchTimeSlotAvailableException("Not available slots with this duration");
        if (visits1.size() == 0 && Duration.between(startTime, endTime).toMinutes() > timeSlot.getDuration())
            if (timeSlot.getStartTime().isBefore(startTime))
                return new TimeSlot().setStartTime(startTime).setDuration(timeSlot.getDuration());
            else if (timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isAfter(endTime))
                return new TimeSlot().setStartTime(endTime.minusMinutes(timeSlot.getDuration())).setDuration(timeSlot.getDuration());
            else
                return timeSlot;
        if (timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isBefore(timeSlot.getStartTime())) {
            timeSlot.setStartTime(endTime.minusMinutes(timeSlot.getDuration()));
        }
        List<LocalTime> tempList = new ArrayList<>();
        Iterator<Visit> iterator = visits1.iterator();
        LocalTime tempStartTime = startTime;
        Visit next = null;
        while (iterator.hasNext()){
            next = iterator.next();
            LocalTime tempEndTime = next.getStartTime();
            addTimeSlotToList(timeSlot, tempList, tempStartTime, tempEndTime);
            tempStartTime = next.getEndTime();
        }
        if (next!=null){
            addTimeSlotToList(timeSlot, tempList, tempStartTime, endTime);
        }

        LocalTime localTime = tempList.stream().min((o2, o1) -> (int) (Math.abs(Duration.between(o2, timeSlot.getStartTime()).toMinutes()) - Math.abs(Duration.between(o1, timeSlot.getStartTime()).toMinutes()))).orElseThrow(() -> new NoSuchTimeSlotAvailableException("Not available slots with this duration"));
        return new TimeSlot().setStartTime(localTime).setDuration(timeSlot.getDuration());
    }

    private void addTimeSlotToList(TimeSlot timeSlot, List<LocalTime> tempList, LocalTime tempStartTime, LocalTime endTime) {
        if (Duration.between(tempStartTime, endTime).toMinutes() >= timeSlot.getDuration()) {
            if (timeSlot.getStartTime().isBefore(tempStartTime))
                tempList.add(tempStartTime);
            else if (timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isAfter(endTime))
                tempList.add(endTime.minusMinutes(timeSlot.getDuration()));
            else
                tempList.add(timeSlot.getStartTime());
        }
    }


    public boolean isTimeSlotAvailable(TimeSlot timeSlot) {
        if (timeSlot.getStartTime().isBefore(startTime) || timeSlot.getStartTime().plusMinutes(timeSlot.getDuration())
                .isAfter(endTime) || Duration.between(startTime, endTime).toMinutes() < timeSlot.getDuration())
            return false;
        if (timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isBefore(timeSlot.getStartTime()))
            return false;
        Iterator<Visit> iterator = visits.stream().filter(e-> !e.getStatus().equals("cancel")).iterator();
        LocalTime freeSlotEndTime;
        LocalTime freeSlotStartTime = startTime;
        Visit next = null;
        while (iterator.hasNext()) {
            next = iterator.next();
            freeSlotEndTime = next.getStartTime();
            if (!timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isAfter(freeSlotEndTime)) {
                return !freeSlotStartTime.isAfter(timeSlot.getStartTime());
            }
            freeSlotStartTime = next.getEndTime();
        }
        if (next != null) {
            if (!timeSlot.getStartTime().plusMinutes(timeSlot.getDuration()).isAfter(endTime)) {
                return !freeSlotStartTime.isAfter(timeSlot.getStartTime());
            }
        }

        return true;
    }

    public List<TimeSlot> getAvailableTimeSlots() {
        List<TimeSlot> tempList = new ArrayList<>();
        Iterator<Visit> iterator = visits.stream().filter(e-> !e.getStatus().equals("cancel")).iterator();
        LocalTime tempStartTime = startTime;
        Visit next;
        while (iterator.hasNext()){
            next = iterator.next();
            if (tempStartTime.isBefore(next.getStartTime())){
                tempList.add(new TimeSlot().setStartTime(tempStartTime).setDuration((int)Duration.between(tempStartTime,next.getStartTime()).toMinutes()));
            }
            tempStartTime = next.getEndTime();
        }
        if (tempStartTime.isBefore(endTime)){
            tempList.add(new TimeSlot().setStartTime(tempStartTime).setDuration((int)Duration.between(tempStartTime,endTime).toMinutes()));
        }

        return tempList;
    }

    public TimeSlot getFirstTimeSlot(int duration) throws NoSuchTimeSlotAvailableException {
        return getNearestTimeSlot(new TimeSlot().setStartTime(startTime).setDuration(duration));
    }

    public UUID getUuid() {
        return uuid;
    }

    public Work setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public Work setStartTime(LocalTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public Work setEndTime(LocalTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public Set<Visit> getVisits() {
        return visits;
    }

    public LocalDate getDate() {
        return date;
    }

    public Work setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public UUID getDoctor_id() {
        return doctor_id;
    }

    public void setVisits(Set<Visit> visits) {
        this.visits = visits;
    }

    public Work setDoctor_id(UUID doctor_id) {
        this.doctor_id = doctor_id;
        return this;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Work setDoctor(Doctor doctor) {
        this.doctor = doctor;
        return this;
    }
    public Map<String,String> isValid(){
        Map<String,String> errors = new HashMap<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Work>> violations = validator.validate(this);
        for (ConstraintViolation<Work> temp :
                violations) {
            errors.put(temp.getPropertyPath().toString(),temp.getMessage());
        }
        return errors;
    }

}
