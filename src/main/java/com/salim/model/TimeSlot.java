package com.salim.model;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.*;

public class TimeSlot {
    private UUID uuid;
    @NotNull
    private LocalTime startTime;

    @NotNull
    @Min(1)
    private int duration;

    public TimeSlot() {
    }

    public Map<String,String> isValid(){
        Map<String,String> errors = new HashMap<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<TimeSlot>> violations = validator.validate(this);
        for (ConstraintViolation<TimeSlot> temp :
                violations) {
            errors.put(temp.getPropertyPath().toString(),temp.getMessage());
        }
        return errors;
    }


    public TimeSlot setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public TimeSlot setStartTime(LocalTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public int getDuration() {
        return duration;
    }

    public TimeSlot setDuration(int duration) {
        this.duration = duration;
        return this;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSlot timeSlot = (TimeSlot) o;
        return duration == timeSlot.duration &&
                Objects.equals(startTime, timeSlot.startTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, duration);
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "uuid=" + uuid +
                ", startTime=" + startTime +
                ", duration=" + duration +
                '}';
    }
}
