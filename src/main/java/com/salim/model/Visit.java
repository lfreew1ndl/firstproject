package com.salim.model;

import com.salim.validator.SimpleVisit;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Salim Abdul on 29.10.2018.
 */

@com.salim.validator.Visit
@SimpleVisit
public class Visit implements Comparable<Visit>{
    private UUID uuid;
    private User user;
    private TimeSlot timeSlot;
    private Work work;
    private String status;

    public Visit() {
    }


    public String getStatus() {
        return status;
    }


    public Visit setStatus(String status) {
        this.status = status;
        return this;
    }

    public UUID getUuid() {
        return uuid;
    }

    public User getUser() {
        return user;
    }

    public Visit setUser(User user) {
        this.user = user;
        return this;
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    public Visit setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
        return this;
    }

    public Visit setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public LocalTime getStartTime(){ return timeSlot.getStartTime();}

    public LocalTime getEndTime(){
        return timeSlot.getStartTime().plusMinutes(timeSlot.getDuration());
    }


    public Work getWork() {
        return work;
    }

    public Visit setWork(Work work) {
        this.work = work;
        return this;
    }

    @Override
    public int compareTo(Visit o) {
        return this.getStartTime().compareTo(o.getStartTime());
    }

    public Map<String,String> isValid(){
        Map<String,String> errors = new HashMap<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Visit>> violations = validator.validate(this);
        for (ConstraintViolation<Visit> temp :
                violations) {
            errors.put(temp.getPropertyPath().toString(),temp.getMessage());
        }
        return errors;
    }

}
