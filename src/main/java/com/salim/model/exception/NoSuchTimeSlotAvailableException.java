package com.salim.model.exception;

/**
 * Created by Salim Abdul on 29.10.2018.
 */
public class NoSuchTimeSlotAvailableException extends Exception {
    public NoSuchTimeSlotAvailableException(String message) {
        super(message);
    }
}
