package com.salim.model;

import com.salim.model.enums.Roles;
import com.salim.service.UserService;
import com.salim.validator.Birthday;
import org.hibernate.validator.constraints.Length;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

public class User {
    private UUID uuid;

    @NotNull(message = "Login must not be null")
    @Length(min = 8,max = 20)
    private String login;

    @NotNull(message = "Password must not be null")
    private String password;

    @NotNull(message = "First name must not be null")
    private String firstName;

    @NotNull(message = "Last name must not be null")
    private String lastName;

    @Birthday
    @NotNull(message = "Birthday must not be null")
    private LocalDate birthday;

    @Email(message = "Email must be correct")
    private String email;

    private List<Roles> roles = new ArrayList<>();

    public User() {
    }

    public UUID getUuid() {
        return uuid;
    }

    public User setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public User setBirthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public User setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public List<Roles> getRoles() {
        return roles;
    }

    public Map<String,String> isValid(){
        Map<String,String> errors = new HashMap<>();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> violations = validator.validate(this);
        for (ConstraintViolation<User> temp :
                violations) {
            errors.put(temp.getPropertyPath().toString(),temp.getMessage());
        }
        try {
            if (!new UserService().isLoginAvailable(login)){
                errors.put("login","Login is already exist");
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return errors;
    }

    @Override
    public String toString() {
        return "User{" +
                "uuid=" + uuid +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                '}';
    }
}
