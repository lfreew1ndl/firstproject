package com.salim.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Doctor {
    private UUID uuid;
    private String profession;
    private List<Work> workList = new ArrayList<>();
    private User user;
    private Boolean active;

    public Doctor() {
    }

    public UUID getUuid() {
        return uuid;
    }

    public Doctor setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public Boolean getActive() {
        return active;
    }

    public Doctor setActive(Boolean active) {
        this.active = active;
        return this;
    }

    public String getProfession() {
        return profession;
    }

    public Doctor setProfession(String profession) {
        this.profession = profession;
        return this;
    }

    public List<Work> getWorkList() {
        return workList;
    }

    public Doctor setWorkList(List<Work> workList) {
        this.workList = workList;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Doctor setUser(User user) {
        this.user = user;
        return this;
    }
}
