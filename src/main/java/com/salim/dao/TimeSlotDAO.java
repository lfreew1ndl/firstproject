package com.salim.dao;

import com.salim.dataStorage.DataStorageJDBC;
import com.salim.model.TimeSlot;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TimeSlotDAO {

    private final String INSERT = "insert into time_slot (start_time, duration) VALUES (?,?)";
    private final String UPDATE = "update time_slot set start_time = ?, duration = ? where id = ?";
    private final String FIND = "select * from time_slot where id = ?";
    private final String FIND_ALL = "select * from time_slot";
    private final String FIND_ALL_FREE = "select time_slot.* from visit right join time_slot on visit.time_slot_id = time_slot.id where visit IS NULL";
    private final String DELETE_BY_ID = "delete from time_slot where id = ?";

    public void save(TimeSlot timeSlot) throws SQLException, ClassNotFoundException {
        if (timeSlot.getUuid() == null) {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setTime(1, Time.valueOf(timeSlot.getStartTime()));
                preparedStatement.setInt(2, timeSlot.getDuration());
                preparedStatement.executeUpdate();
            }

        } else {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                preparedStatement.setTime(1, Time.valueOf(timeSlot.getStartTime()));
                preparedStatement.setInt(2, timeSlot.getDuration());
                preparedStatement.setObject(3, timeSlot.getUuid());
                preparedStatement.executeUpdate();
            }
        }
    }

    public TimeSlot find(UUID uuid) throws SQLException, ClassNotFoundException {
        TimeSlot timeSlot = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                timeSlot = new TimeSlot().setUuid((UUID) resultSet.getObject("id"))
                        .setStartTime((resultSet.getTime("start_time").toLocalTime()))
                        .setDuration(resultSet.getInt("duration"));
            }
        }

        return timeSlot;
    }

    @SuppressWarnings("Duplicates")
    public List<TimeSlot> findFreeTimeSlots() throws SQLException, ClassNotFoundException {
        List<TimeSlot> timeSlots = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_FREE)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                timeSlots.add(new TimeSlot().setUuid((UUID) resultSet.getObject("id"))
                        .setStartTime((resultSet.getTime("start_time")).toLocalTime())
                        .setDuration(resultSet.getInt("duration")));
            }
        }
        return timeSlots;
    }

    @SuppressWarnings("Duplicates")
    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
            preparedStatement.setObject(1, uuid);
            preparedStatement.executeUpdate();
        }
    }

    @SuppressWarnings("Duplicates")
    public List<TimeSlot> findAll() throws SQLException, ClassNotFoundException {
        List<TimeSlot> timeSlots = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                timeSlots.add(new TimeSlot().setUuid((UUID) resultSet.getObject("id"))
                        .setStartTime((resultSet.getTime("start_time")).toLocalTime())
                        .setDuration(resultSet.getInt("duration")));
            }
        }


        return timeSlots;
    }
}
