package com.salim.dao;

import com.salim.dataStorage.DataStorageJDBC;
import com.salim.model.enums.Roles;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RoleDAO {

    private final String FIND_BY_USER = "select role.role from role join user_role on role.id = user_role.role_id where user_role.user_id = ?";
    private final String FIND_ROLE_BY_NAME = "select role.id from role where role.role = ?";
    private final String SAVE_ROLE_TO_USER = "insert into user_role (user_id, role_id) VALUES (?,?)";
    private final String DELETE_ROLE_FROM_USER = "delete from user_role where user_id = ? and role_id = ?";

    public List<Roles> findRolesByUserId(UUID uuid) throws SQLException, ClassNotFoundException {
        List<Roles> roles = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_USER)) {
            preparedStatement.setObject(1,uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roles.add(Roles.valueOf(resultSet.getString(1)));
            }
        }
        return roles;
    }

    public UUID getRoleByName(Roles role) throws SQLException, ClassNotFoundException {
        UUID uuid = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ROLE_BY_NAME)) {
            preparedStatement.setString(1,role.name());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                uuid = (UUID) resultSet.getObject(1);
            }
        }
        return uuid;
    }

    public void saveRole(UUID uuid, Roles role) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        UUID roleUUID = getRoleByName(role);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_ROLE_TO_USER)) {
            preparedStatement.setObject(1,uuid);
            preparedStatement.setObject(2,roleUUID);
            preparedStatement.executeUpdate();
        }
    }

    public void removeRole(UUID uuid, Roles role) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        UUID roleUUID = getRoleByName(role);
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ROLE_FROM_USER)) {
            preparedStatement.setObject(1,uuid);
            preparedStatement.setObject(2,roleUUID);
            preparedStatement.executeUpdate();
        }
    }
}
