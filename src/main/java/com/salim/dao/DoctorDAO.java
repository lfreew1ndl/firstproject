package com.salim.dao;

import com.salim.dataStorage.DataStorageJDBC;
import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.model.Work;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DoctorDAO {

    private final String INSERT = "insert into doctor (user_id, profession) VALUES (?,?)";
    private final String UPDATE = "update doctor set profession = ?, active = ? where id = ?";
    private final String FIND = "select * from doctor join \"user\" u on doctor.user_id = u.id where doctor.id = ?";
    private final String FIND_BY_USER = "select * from doctor join \"user\" u on doctor.user_id = u.id where u.id = ?";
    private final String FIND_ALL = "select * from doctor join \"user\" u on doctor.user_id = u.id where active = true ";
    private final String DELETE_BY_ID = "delete from doctor where id = ?";

    private WorkDAO workDAO;

    public DoctorDAO() {
        workDAO = new WorkDAO();
    }

    public DoctorDAO(WorkDAO workDAO) {
        this.workDAO = workDAO;
    }

    public void save(Doctor doctor) throws SQLException, ClassNotFoundException {
        if (doctor.getUuid() == null) {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setObject(1, doctor.getUser().getUuid());
                preparedStatement.setString(2, doctor.getProfession());
                preparedStatement.executeUpdate();
            }
        } else {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                preparedStatement.setString(1, doctor.getProfession());
                preparedStatement.setBoolean(2, doctor.getActive());
                preparedStatement.setObject(3, doctor.getUuid());
                preparedStatement.executeUpdate();
            }
        }
    }


    public Doctor find(UUID uuid) throws SQLException, ClassNotFoundException {
        Doctor doctor = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                doctor = new Doctor().setUuid((UUID) resultSet.getObject("id"))
                        .setProfession(resultSet.getString("profession"));
                User user = new User();
                user.setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setEmail(resultSet.getString("email"))
                        .setUuid((UUID) resultSet.getObject("user_id"));
                doctor.setUser(user);
            }
        }
        if (doctor != null) {
            List<Work> byDoctor = workDAO.findByDoctor(uuid);

            doctor.getWorkList().addAll(byDoctor);
        }
        return doctor;
    }


    public Doctor findByUser(UUID uuid) throws SQLException, ClassNotFoundException {
        Doctor doctor = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_USER)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                doctor = new Doctor().setUuid((UUID) resultSet.getObject("id"))
                        .setProfession(resultSet.getString("profession"))
                .setActive(resultSet.getBoolean("active"));

                User user = new User();
                user.setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setEmail(resultSet.getString("email"))
                        .setUuid((UUID) resultSet.getObject("user_id"));
                doctor.setUser(user);

                List<Work> byDoctor = workDAO.findByDoctor(doctor.getUuid());


                doctor.getWorkList().addAll(byDoctor);
            }
        }
        return doctor;
    }

    @SuppressWarnings("Duplicates")
    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
            preparedStatement.setObject(1, uuid);
            preparedStatement.executeUpdate();
        }
    }

    public List<Doctor> findAll() throws SQLException, ClassNotFoundException {
        List<Doctor> doctors = new ArrayList<>();
        Connection connection;

        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Doctor doctor = new Doctor().setUuid((UUID) resultSet.getObject("id"))
                        .setProfession(resultSet.getString("profession"))
                        .setActive(resultSet.getBoolean("active"));
                User user = new User();
                user.setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setEmail(resultSet.getString("email"))
                        .setUuid((UUID) resultSet.getObject("user_id"));
                doctor.setUser(user);
                doctors.add(doctor);
            }
        }

        for (Doctor doctor :
                doctors) {
            doctor.getWorkList().addAll(workDAO.findByDoctor(doctor.getUuid()));
        }


        return doctors;
    }

}
