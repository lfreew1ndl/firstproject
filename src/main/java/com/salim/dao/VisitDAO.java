package com.salim.dao;

import com.salim.dataStorage.DataStorageJDBC;
import com.salim.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VisitDAO {

    private final String INSERT = "insert into visit (user_id, work_id, time_slot_id) VALUES (?,?,?)";
    private final String UPDATE = "update visit set user_id = ?, work_id = ?, time_slot_id = ?, status = ? where id = ?";
    private final String FIND = "select * from visit join work w2 on visit.work_id = w2.id join time_slot t on visit.time_slot_id = t.id join \"user\" u on visit.user_id = u.id join doctor d2 on w2.doctor_id = d2.id join \"user\" u2 on d2.user_id = u2.id where visit.id = ?";
    private final String FIND_VISITS_BY_USER = "select * from visit join work w2 on visit.work_id = w2.id join time_slot t on visit.time_slot_id = t.id join \"user\" u on visit.user_id = u.id join doctor d2 on w2.doctor_id = d2.id join \"user\" u2 on d2.user_id = u2.id where visit.user_id = ?";
    private final String FIND_VISITS_BY_DOCTOR = "select * from visit join work w2 on visit.work_id = w2.id join time_slot t on visit.time_slot_id = t.id join \"user\" u on visit.user_id = u.id join doctor d2 on w2.doctor_id = d2.id join \"user\" u2 on d2.user_id = u2.id where w2.doctor_id = ?";
    private final String FIND_ALL = "select * from visit join work w2 on visit.work_id = w2.id join time_slot t on visit.time_slot_id = t.id join \"user\" u on visit.user_id = u.id join doctor d2 on w2.doctor_id = d2.id join \"user\" u2 on d2.user_id = u2.id where status = ?";
    private final String FIND_ALL_BY_STATUS = "select * from visit join work w2 on visit.work_id = w2.id join time_slot t on visit.time_slot_id = t.id join \"user\" u on visit.user_id = u.id join doctor d2 on w2.doctor_id = d2.id join \"user\" u2 on d2.user_id = u2.id where status = ?";
    private final String FIND_BY_WORK = "select * from visit join work w2 on visit.work_id = w2.id join time_slot t on visit.time_slot_id = t.id join \"user\" u on visit.user_id = u.id join doctor d2 on w2.doctor_id = d2.id join \"user\" u2 on d2.user_id = u2.id where w2.id= ?";
    private final String DELETE_BY_ID = "delete from visit where id = ?";

    private UserDAO userDAO = new UserDAO();
    private TimeSlotDAO timeSlotDAO = new TimeSlotDAO();
    private WorkDAO workDAO;

    public VisitDAO() {
        this.workDAO = new WorkDAO(this);
    }

    public VisitDAO(WorkDAO workDAO) {
        this.workDAO = workDAO;
    }

    public void save(Visit visit) throws SQLException, ClassNotFoundException {
        if (visit.getUuid() == null) {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setObject(1, visit.getUser().getUuid());
                preparedStatement.setObject(2, visit.getWork().getUuid());
                preparedStatement.setObject(3, visit.getTimeSlot().getUuid());
                preparedStatement.executeUpdate();
            }

        } else {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                preparedStatement.setObject(1, visit.getUser().getUuid());
                preparedStatement.setObject(2, visit.getWork().getUuid());
                preparedStatement.setObject(3, visit.getTimeSlot().getUuid());
                preparedStatement.setString(4, visit.getStatus());
                preparedStatement.setObject(5, visit.getUuid());
                preparedStatement.executeUpdate();
            }

        }
    }

    public Visit find(UUID uuid) throws SQLException, ClassNotFoundException {
        Visit visit = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                visit = new Visit().setUuid((UUID) resultSet.getObject(1))
                        .setStatus(resultSet.getString(5));

                Work work = new Work().setUuid((UUID) resultSet.getObject(6))
                        .setDate(resultSet.getDate(7).toLocalDate())
                        .setStartTime(resultSet.getTime(8).toLocalTime())
                        .setEndTime(resultSet.getTime(9).toLocalTime())
                        .setActive(resultSet.getBoolean(11));

                TimeSlot timeSlot = new TimeSlot().setUuid((UUID)resultSet.getObject(12))
                        .setStartTime(resultSet.getTime(13).toLocalTime())
                        .setDuration(resultSet.getInt(14));

                User userInVisit = new User().setUuid((UUID)resultSet.getObject(15))
                        .setLogin(resultSet.getString(16))
                        .setPassword(resultSet.getString(17))
                        .setFirstName(resultSet.getString(18))
                        .setLastName(resultSet.getString(19))
                        .setBirthday(resultSet.getDate(20).toLocalDate())
                        .setEmail(resultSet.getString(21));
                visit.setWork(work).setUser(userInVisit).setTimeSlot(timeSlot);


                Doctor doctor = new Doctor().setUuid((UUID)resultSet.getObject(22))
                        .setProfession(resultSet.getString(24))
                        .setActive(resultSet.getBoolean(25));

                work.setDoctor(doctor);

                User user = new User().setUuid((UUID) resultSet.getObject(26))
                        .setLogin(resultSet.getString(27))
                        .setPassword(resultSet.getString(28))
                        .setFirstName(resultSet.getString(29))
                        .setLastName(resultSet.getString(30))
                        .setBirthday(resultSet.getDate(31).toLocalDate())
                        .setEmail(resultSet.getString(32));
                doctor.setUser(user);
            }
        }
        return visit;
    }

    @SuppressWarnings("Duplicates")
    public List<Visit> findByUser(UUID uuid) throws SQLException, ClassNotFoundException {
        List<Visit> visits = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_VISITS_BY_USER)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Visit visit = new Visit().setUuid((UUID) resultSet.getObject(1))
                        .setStatus(resultSet.getString(5));

                Work work = new Work().setUuid((UUID) resultSet.getObject(6))
                        .setDate(resultSet.getDate(7).toLocalDate())
                        .setStartTime(resultSet.getTime(8).toLocalTime())
                        .setEndTime(resultSet.getTime(9).toLocalTime())
                        .setActive(resultSet.getBoolean(11));

                TimeSlot timeSlot = new TimeSlot().setUuid((UUID)resultSet.getObject(12))
                        .setStartTime(resultSet.getTime(13).toLocalTime())
                        .setDuration(resultSet.getInt(14));

                User userInVisit = new User().setUuid((UUID)resultSet.getObject(15))
                        .setLogin(resultSet.getString(16))
                        .setPassword(resultSet.getString(17))
                        .setFirstName(resultSet.getString(18))
                        .setLastName(resultSet.getString(19))
                        .setBirthday(resultSet.getDate(20).toLocalDate())
                        .setEmail(resultSet.getString(21));
                visit.setWork(work).setUser(userInVisit).setTimeSlot(timeSlot);


                Doctor doctor = new Doctor().setUuid((UUID)resultSet.getObject(22))
                        .setProfession(resultSet.getString(24))
                        .setActive(resultSet.getBoolean(25));

                work.setDoctor(doctor);

                User user = new User().setUuid((UUID) resultSet.getObject(26))
                        .setLogin(resultSet.getString(27))
                        .setPassword(resultSet.getString(28))
                        .setFirstName(resultSet.getString(29))
                        .setLastName(resultSet.getString(30))
                        .setBirthday(resultSet.getDate(31).toLocalDate())
                        .setEmail(resultSet.getString(32));
                doctor.setUser(user);
                visits.add(visit);
            }
        }

        return visits;
    }

    @SuppressWarnings("Duplicates")
    public List<Visit> findByDoctor(UUID uuid) throws SQLException, ClassNotFoundException {
        List<Visit> visits = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_VISITS_BY_DOCTOR)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Visit visit = new Visit().setUuid((UUID) resultSet.getObject(1))
                        .setStatus(resultSet.getString(5));

                Work work = new Work().setUuid((UUID) resultSet.getObject(6))
                        .setDate(resultSet.getDate(7).toLocalDate())
                        .setStartTime(resultSet.getTime(8).toLocalTime())
                        .setEndTime(resultSet.getTime(9).toLocalTime())
                        .setActive(resultSet.getBoolean(11));

                TimeSlot timeSlot = new TimeSlot().setUuid((UUID)resultSet.getObject(12))
                        .setStartTime(resultSet.getTime(13).toLocalTime())
                        .setDuration(resultSet.getInt(14));

                User userInVisit = new User().setUuid((UUID)resultSet.getObject(15))
                        .setLogin(resultSet.getString(16))
                        .setPassword(resultSet.getString(17))
                        .setFirstName(resultSet.getString(18))
                        .setLastName(resultSet.getString(19))
                        .setBirthday(resultSet.getDate(20).toLocalDate())
                        .setEmail(resultSet.getString(21));
                visit.setWork(work).setUser(userInVisit).setTimeSlot(timeSlot);


                Doctor doctor = new Doctor().setUuid((UUID)resultSet.getObject(22))
                        .setProfession(resultSet.getString(24))
                        .setActive(resultSet.getBoolean(25));

                work.setDoctor(doctor);

                User user = new User().setUuid((UUID) resultSet.getObject(26))
                        .setLogin(resultSet.getString(27))
                        .setPassword(resultSet.getString(28))
                        .setFirstName(resultSet.getString(29))
                        .setLastName(resultSet.getString(30))
                        .setBirthday(resultSet.getDate(31).toLocalDate())
                        .setEmail(resultSet.getString(32));
                doctor.setUser(user);
                visits.add(visit);
            }
        }

        return visits;
    }

    @SuppressWarnings("Duplicates")
    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
            preparedStatement.setObject(1, uuid);
            preparedStatement.executeUpdate();
        }

    }

    @SuppressWarnings("Duplicates")
    public List<Visit> findAll() throws SQLException, ClassNotFoundException {
        List<Visit> visits = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Visit visit = new Visit().setUuid((UUID) resultSet.getObject(1))
                        .setStatus(resultSet.getString(5));

                Work work = new Work().setUuid((UUID) resultSet.getObject(6))
                        .setDate(resultSet.getDate(7).toLocalDate())
                        .setStartTime(resultSet.getTime(8).toLocalTime())
                        .setEndTime(resultSet.getTime(9).toLocalTime())
                        .setActive(resultSet.getBoolean(11));

                TimeSlot timeSlot = new TimeSlot().setUuid((UUID)resultSet.getObject(12))
                        .setStartTime(resultSet.getTime(13).toLocalTime())
                        .setDuration(resultSet.getInt(14));

                User userInVisit = new User().setUuid((UUID)resultSet.getObject(15))
                        .setLogin(resultSet.getString(16))
                        .setPassword(resultSet.getString(17))
                        .setFirstName(resultSet.getString(18))
                        .setLastName(resultSet.getString(19))
                        .setBirthday(resultSet.getDate(20).toLocalDate())
                        .setEmail(resultSet.getString(21));
                visit.setWork(work).setUser(userInVisit).setTimeSlot(timeSlot);


                Doctor doctor = new Doctor().setUuid((UUID)resultSet.getObject(22))
                        .setProfession(resultSet.getString(24))
                        .setActive(resultSet.getBoolean(25));

                work.setDoctor(doctor);

                User user = new User().setUuid((UUID) resultSet.getObject(26))
                        .setLogin(resultSet.getString(27))
                        .setPassword(resultSet.getString(28))
                        .setFirstName(resultSet.getString(29))
                        .setLastName(resultSet.getString(30))
                        .setBirthday(resultSet.getDate(31).toLocalDate())
                        .setEmail(resultSet.getString(32));
                doctor.setUser(user);
                visits.add(visit);
            }
        }

        return visits;
    }

    @SuppressWarnings("Duplicates")
    public List<Visit> findAll(String status) throws SQLException, ClassNotFoundException {
        List<Visit> visits = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_BY_STATUS)) {
            preparedStatement.setString(1, status);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Visit visit = new Visit().setUuid((UUID) resultSet.getObject(1))
                        .setStatus(resultSet.getString(5));

                Work work = new Work().setUuid((UUID) resultSet.getObject(6))
                        .setDate(resultSet.getDate(7).toLocalDate())
                        .setStartTime(resultSet.getTime(8).toLocalTime())
                        .setEndTime(resultSet.getTime(9).toLocalTime())
                        .setActive(resultSet.getBoolean(11));

                TimeSlot timeSlot = new TimeSlot().setUuid((UUID)resultSet.getObject(12))
                        .setStartTime(resultSet.getTime(13).toLocalTime())
                        .setDuration(resultSet.getInt(14));

                User userInVisit = new User().setUuid((UUID)resultSet.getObject(15))
                        .setLogin(resultSet.getString(16))
                        .setPassword(resultSet.getString(17))
                        .setFirstName(resultSet.getString(18))
                        .setLastName(resultSet.getString(19))
                        .setBirthday(resultSet.getDate(20).toLocalDate())
                        .setEmail(resultSet.getString(21));
                visit.setWork(work).setUser(userInVisit).setTimeSlot(timeSlot);


                Doctor doctor = new Doctor().setUuid((UUID)resultSet.getObject(22))
                        .setProfession(resultSet.getString(24))
                        .setActive(resultSet.getBoolean(25));

                work.setDoctor(doctor);

                User user = new User().setUuid((UUID) resultSet.getObject(26))
                        .setLogin(resultSet.getString(27))
                        .setPassword(resultSet.getString(28))
                        .setFirstName(resultSet.getString(29))
                        .setLastName(resultSet.getString(30))
                        .setBirthday(resultSet.getDate(31).toLocalDate())
                        .setEmail(resultSet.getString(32));
                doctor.setUser(user);
                visits.add(visit);
            }
        }

        return visits;
    }

    @SuppressWarnings("Duplicates")
    public List<Visit> findByWork(UUID uuid) throws SQLException, ClassNotFoundException {
        List<Visit> visits = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_WORK)) {

            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Visit                 visit = new Visit().setUuid((UUID) resultSet.getObject(1))
                        .setStatus(resultSet.getString(5));

                Work work = new Work().setUuid((UUID) resultSet.getObject(6))
                        .setDate(resultSet.getDate(7).toLocalDate())
                        .setStartTime(resultSet.getTime(8).toLocalTime())
                        .setEndTime(resultSet.getTime(9).toLocalTime())
                        .setActive(resultSet.getBoolean(11));

                TimeSlot timeSlot = new TimeSlot().setUuid((UUID)resultSet.getObject(12))
                        .setStartTime(resultSet.getTime(13).toLocalTime())
                        .setDuration(resultSet.getInt(14));

                User userInVisit = new User().setUuid((UUID)resultSet.getObject(15))
                        .setLogin(resultSet.getString(16))
                        .setPassword(resultSet.getString(17))
                        .setFirstName(resultSet.getString(18))
                        .setLastName(resultSet.getString(19))
                        .setBirthday(resultSet.getDate(20).toLocalDate())
                        .setEmail(resultSet.getString(21));
                visit.setWork(work).setUser(userInVisit).setTimeSlot(timeSlot);


                Doctor doctor = new Doctor().setUuid((UUID)resultSet.getObject(22))
                        .setProfession(resultSet.getString(24))
                        .setActive(resultSet.getBoolean(25));

                work.setDoctor(doctor);

                User user = new User().setUuid((UUID) resultSet.getObject(26))
                        .setLogin(resultSet.getString(27))
                        .setPassword(resultSet.getString(28))
                        .setFirstName(resultSet.getString(29))
                        .setLastName(resultSet.getString(30))
                        .setBirthday(resultSet.getDate(31).toLocalDate())
                        .setEmail(resultSet.getString(32));
                doctor.setUser(user);
                visits.add(visit);
            }
        }
        return visits;
    }
}
