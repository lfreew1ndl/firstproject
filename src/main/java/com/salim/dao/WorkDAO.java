package com.salim.dao;

import com.salim.dataStorage.DataStorageJDBC;
import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.model.Work;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WorkDAO {

    private final String INSERT = "insert into work (date, start_time, end_time, doctor_id) VALUES (?,?,?,?)";
    private final String UPDATE = "update work set date = ?, start_time = ?, end_time = ?, active = ? where id = ?";
    private final String FIND = "select * from work join doctor d2 on work.doctor_id = d2.id join \"user\" u on d2.user_id = u.id where work.id = ?";
    private final String FIND_BY_DOCTOR = "select * from work join doctor d2 on work.doctor_id = d2.id join \"user\" u on d2.user_id = u.id where doctor_id = ?";
    private final String FIND_ALL = "select * from work join doctor d2 on work.doctor_id = d2.id join \"user\" u on d2.user_id = u.id ";
    private final String DELETE_BY_ID = "delete from work where id = ?";

    private VisitDAO visitDAO;

    public WorkDAO() {
        visitDAO = new VisitDAO(this);
    }

    public WorkDAO(VisitDAO visitDAO) {
        this.visitDAO = visitDAO;
    }

    public void save(Work work) throws SQLException, ClassNotFoundException {
        if (work.getUuid() == null) {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setDate(1, Date.valueOf(work.getDate()));
                preparedStatement.setTime(2, Time.valueOf(work.getStartTime()));
                preparedStatement.setTime(3, Time.valueOf(work.getEndTime()));
                preparedStatement.setObject(4, work.getDoctor_id());
                preparedStatement.executeUpdate();
            }

        } else {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                preparedStatement.setDate(1, Date.valueOf(work.getDate()));
                preparedStatement.setTime(2, Time.valueOf(work.getStartTime()));
                preparedStatement.setTime(3, Time.valueOf(work.getEndTime()));
                preparedStatement.setObject(4, work.isActive());
                preparedStatement.setObject(5, work.getUuid());
                preparedStatement.executeUpdate();
            }
        }
    }

    public Work find(UUID uuid) throws SQLException, ClassNotFoundException {
        Work work = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                work = new Work().setUuid((UUID) resultSet.getObject("id"))
                        .setStartTime((resultSet.getTime("start_time").toLocalTime()))
                        .setEndTime((resultSet.getTime("end_time").toLocalTime()))
                        .setDate((resultSet.getDate("date").toLocalDate()))
                        .setActive(resultSet.getBoolean("active"))
                        .setDoctor_id((UUID) resultSet.getObject("doctor_id"));
                Doctor doctor = new Doctor().setUuid((UUID) resultSet.getObject("id"))
                        .setProfession(resultSet.getString("profession"));
                User user = new User();
                user.setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setEmail(resultSet.getString("email"))
                        .setUuid((UUID) resultSet.getObject("user_id"));
                doctor.setUser(user);
                work.setDoctor(doctor);
            }
        }
        if(work!= null){
            work.getVisits().addAll(visitDAO.findByWork(work.getUuid()));
        }

        return work;
    }

    @SuppressWarnings("Duplicates")
    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
            preparedStatement.setObject(1, uuid);
            preparedStatement.executeUpdate();
        }
    }

    public List<Work> findAll() throws SQLException, ClassNotFoundException {
        List<Work> works = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Work work= new Work().setUuid((UUID) resultSet.getObject("id"))
                        .setStartTime((resultSet.getTime("start_time").toLocalTime()))
                        .setEndTime((resultSet.getTime("end_time").toLocalTime()))
                        .setDate((resultSet.getDate("date").toLocalDate()))
                        .setActive(resultSet.getBoolean("active"))
                        .setDoctor_id((UUID) resultSet.getObject("doctor_id"));
                Doctor doctor = new Doctor().setUuid((UUID) resultSet.getObject("id"))
                        .setProfession(resultSet.getString("profession"));
                User user = new User();
                user.setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setEmail(resultSet.getString("email"))
                        .setUuid((UUID) resultSet.getObject("user_id"));
                doctor.setUser(user);
                work.setDoctor(doctor);
                works.add(work);
            }
        }


        return works;
    }

    public List<Work> findByDoctor(UUID uuid) throws SQLException, ClassNotFoundException {
        List<Work> works = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_DOCTOR)) {

            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Work work = new Work().setUuid((UUID) resultSet.getObject("id"))
                        .setStartTime((resultSet.getTime("start_time").toLocalTime()))
                        .setEndTime((resultSet.getTime("end_time").toLocalTime()))
                        .setDate((resultSet.getDate("date").toLocalDate()))
                        .setActive(resultSet.getBoolean("active"))
                        .setDoctor_id((UUID) resultSet.getObject("doctor_id"));
                works.add(work);
            }
        }

        for (Work work :
                works)
            work.getVisits().addAll(visitDAO.findByWork(work.getUuid()));
        return works;
    }
}
