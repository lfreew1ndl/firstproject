package com.salim.dao;

import com.salim.crypt.Crypter;
import com.salim.dataStorage.DataStorageJDBC;
import com.salim.model.User;
import com.salim.model.enums.Roles;
import com.salim.service.RoleService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserDAO {

    private final String INSERT = "insert into \"user\" (login,password,first_name, last_name, birthday,email) VALUES (?,?,?,?,?,?)";
    private final String UPDATE = "update \"user\" set password = ?, first_name = ?, last_name = ?, birthday = ? where id = ?";
    private final String FIND = "select * from \"user\" where id = ?";
    private final String FIND_BY_LOGIN = "select * from \"user\" where login = ?";
    private final String COUNT_OF_PATIENT_WITH_ROLE = "select count(*) from \"user\" right join user_role ur on ur.user_id = \"user\".id join role r on r.id = ur.role_id where r.role = ?";
    private final String FIND_USER_BY_LOGIN = "select * from \"user\" where login = ?";
    private final String FIND_ALL_FREE = "select u.* from doctor right join \"user\" u on doctor.user_id = u.id where doctor IS NULL or doctor.active = false ";
    private final String FIND_ALL = "select * from \"user\"";
    private final String DELETE_BY_ID = "delete from \"user\" where id = ?";

    private RoleService roleService = new RoleService();

    public void save(User user) throws SQLException, ClassNotFoundException {
        if (user.getUuid() == null) {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.setString(2, Crypter.getHashPassword(user.getPassword()));
                preparedStatement.setString(3, user.getFirstName());
                preparedStatement.setString(4, user.getLastName());
                preparedStatement.setDate(5, Date.valueOf(user.getBirthday()));
                preparedStatement.setObject(6, user.getEmail());
                preparedStatement.executeUpdate();
            }

        } else {
            Connection connection;
            connection = DataStorageJDBC.getConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                preparedStatement.setString(1, user.getPassword());
                preparedStatement.setString(2, user.getFirstName());
                preparedStatement.setString(3, user.getLastName());
                preparedStatement.setDate(4, Date.valueOf(user.getBirthday()));
                preparedStatement.setObject(5, user.getUuid());
                preparedStatement.executeUpdate();
            }
        }
    }

    public int countOfUsers(Roles role) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(COUNT_OF_PATIENT_WITH_ROLE)) {
            preparedStatement.setString(1, role.name());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        }
        return 0;
    }

    public boolean isLoginAvailable(String login) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("Duplicates")
    public User find(UUID uuid) throws SQLException, ClassNotFoundException {
        User user = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User().setUuid((UUID) resultSet.getObject("id"))
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setEmail(resultSet.getString("email"));
            }
        }
        if (user != null) {
            List<Roles> rolesByUserId = roleService.findRolesByUserId(user.getUuid());
            for (Roles role :
                    rolesByUserId) {
                user.getRoles().add(role);
            }
        }
        return user;
    }

    @SuppressWarnings("Duplicates")
    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
            preparedStatement.setObject(1, uuid);
            preparedStatement.executeUpdate();
        }
    }

    @SuppressWarnings("Duplicates")
    public List<User> findFreeUsers() throws SQLException, ClassNotFoundException {
        List<User> users = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_FREE)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                users.add(new User().setUuid((UUID) resultSet.getObject("id"))
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setEmail(resultSet.getString("email")));
            }
        }
        return users;
    }

    @SuppressWarnings("Duplicates")
    public List<User> findAll() throws SQLException, ClassNotFoundException {
        List<User> users = new ArrayList<>();
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                users.add(new User().setUuid((UUID) resultSet.getObject("id"))
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setEmail(resultSet.getString("email")));
            }
        }

        return users;
    }

    @SuppressWarnings("Duplicates")
    public User login(String login) throws SQLException, ClassNotFoundException {
        User user = null;
        Connection connection;
        connection = DataStorageJDBC.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_LOGIN)) {
            preparedStatement.setObject(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User().setUuid((UUID) resultSet.getObject("id"))
                        .setLogin(resultSet.getString("login"))
                        .setPassword(resultSet.getString("password"))
                        .setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setBirthday(resultSet.getDate("birthday").toLocalDate())
                        .setEmail(resultSet.getString("email"));
            }
        }
        if (user != null) {
            List<Roles> rolesByUserId = roleService.findRolesByUserId(user.getUuid());
            for (Roles role :
                    rolesByUserId) {
                user.getRoles().add(role);
            }
        }

        return user;
    }
}
