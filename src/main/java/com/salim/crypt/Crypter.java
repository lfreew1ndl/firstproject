package com.salim.crypt;

import org.mindrot.jbcrypt.BCrypt;

public class Crypter  extends BCrypt {
    private static final int WORK_FACTOR = 15;

    private Crypter() {
        throw new IllegalStateException("Utility Class");
    }

    public static String getHashPassword(String password) {
        String salt = BCrypt.gensalt(WORK_FACTOR);
        return BCrypt.hashpw(password, salt);
    }

    public static boolean checkPassword(String plainPassword, String hashedPassword) {
        return !BCrypt.checkpw(plainPassword, hashedPassword);
    }

}
