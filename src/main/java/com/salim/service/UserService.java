package com.salim.service;

import com.salim.crypt.Crypter;
import com.salim.model.User;
import com.salim.model.enums.Roles;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.salim.dao.UserDAO;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class UserService {
    private final Logger logger = LogManager.getLogger(DoctorService.class);
    private UserDAO userDAO = new UserDAO();

    public User find(UUID uuid) throws SQLException, ClassNotFoundException {
        return userDAO.find(uuid);
    }

    public boolean isLoginAvailable(String login) throws SQLException, ClassNotFoundException {
       return userDAO.isLoginAvailable(login);
    }

    public User find(User user) throws SQLException, ClassNotFoundException {
        return find(user.getUuid());
    }

    public int countOfUsers(Roles role) throws SQLException, ClassNotFoundException {
        return userDAO.countOfUsers(role);
    }

    public List<User> findFreeUsers() throws SQLException, ClassNotFoundException {
        return userDAO.findFreeUsers();
    }

    public void save(User user) throws SQLException, ClassNotFoundException {
        userDAO.save(user);
    }

    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        userDAO.deleteById(uuid);
    }

    public void delete(User user) throws SQLException, ClassNotFoundException {
        deleteById(user.getUuid());
    }

    public List<User> findAll() throws SQLException, ClassNotFoundException {
        return userDAO.findAll();
    }

    public User login(User user) throws SQLException, ClassNotFoundException {
        User login = userDAO.login(user.getLogin());

        if (login != null && !Crypter.checkPassword(user.getPassword(),login.getPassword()))
            return login;
        return null;
    }
}
