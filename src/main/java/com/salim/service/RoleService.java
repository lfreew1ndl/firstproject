package com.salim.service;

import com.salim.dao.RoleDAO;
import com.salim.model.User;
import com.salim.model.enums.Roles;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RoleService {
    private RoleDAO roleDAO = new RoleDAO();
    public List<Roles> findRolesByUserId(UUID uuid) throws SQLException, ClassNotFoundException {
        return roleDAO.findRolesByUserId(uuid);
    }

    public void saveRolesToUser(User user) throws SQLException, ClassNotFoundException {
        List<Roles> rolesByUserId = roleDAO.findRolesByUserId(user.getUuid());

        List<Roles> finalRolesByUserId = rolesByUserId;
        List<Roles> rolesStream = user.getRoles().stream().filter((e->!finalRolesByUserId.contains(e))).collect(Collectors.toList());
        for (Roles role :
                rolesStream) {
            roleDAO.saveRole(user.getUuid(),role);
        }
        rolesByUserId = roleDAO.findRolesByUserId(user.getUuid());
        rolesStream = rolesByUserId.stream().filter((e->!user.getRoles().contains(e))).collect(Collectors.toList());
        for (Roles role :
                rolesStream) {
            roleDAO.removeRole(user.getUuid(),role);
        }
    }
}
