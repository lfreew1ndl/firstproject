package com.salim.service;

import com.salim.dao.WorkDAO;
import com.salim.model.Doctor;
import com.salim.model.Work;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class WorkService {
    private final Logger logger = LogManager.getLogger(DoctorService.class);
    private WorkDAO workDAO = new WorkDAO();

    public Work find(UUID uuid) throws SQLException, ClassNotFoundException {
        return workDAO.find(uuid);
    }

    public Work find(Work work) throws SQLException, ClassNotFoundException {
        return find(work.getUuid());
    }

    public void save(Work work) throws SQLException, ClassNotFoundException {
        workDAO.save(work);
    }

    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        workDAO.deleteById(uuid);
    }

    public void delete(Work work) throws SQLException, ClassNotFoundException {
        deleteById(work.getUuid());
    }

    public List<Work> findAll() throws SQLException, ClassNotFoundException {
        List<Work> all = workDAO.findAll();
        return all;
    }

    public List<Work> findByDoctor(UUID uuid) throws SQLException, ClassNotFoundException {
        return workDAO.findByDoctor(uuid);
    }

    public List<Work> findByDoctor(Doctor doctor) throws SQLException, ClassNotFoundException {
        return findByDoctor(doctor.getUuid());
    }
}
