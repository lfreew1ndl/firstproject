package com.salim.service;

import com.salim.dao.*;
import com.salim.model.*;
import com.salim.model.enums.Roles;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.salim.model.exception.NoSuchDoctorException;
import com.salim.model.exception.NoSuchTimeSlotAvailableException;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


public class DoctorService {


    private final Logger logger = LogManager.getLogger(DoctorService.class);
    private DoctorDAO doctorDAO = new DoctorDAO();
    private UserDAO userDao = new UserDAO();
    private RoleService roleService = new RoleService();
    private WorkDAO workDAO = new WorkDAO();
    private VisitDAO visitDAO = new VisitDAO();
    private TimeSlotDAO timeSlotDAO = new TimeSlotDAO();

    public Doctor find(UUID uuid) throws SQLException, ClassNotFoundException {
        return doctorDAO.find(uuid);
    }

    public Doctor find(Doctor doctor) throws SQLException, ClassNotFoundException {
        return find(doctor.getUuid());
    }

    public void save(Doctor doctor) throws ClassNotFoundException, SQLException {
        try {
            doctorDAO.save(doctor);
        } catch (SQLException e) {
            Doctor byUser = doctorDAO.findByUser(doctor.getUser().getUuid());
            byUser.setActive(true);
            doctorDAO.save(byUser);
        }
    }

    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        doctorDAO.deleteById(uuid);
    }

    public void delete(Doctor doctor) throws SQLException, ClassNotFoundException {
        deleteById(doctor.getUuid());
    }

    public List<Doctor> findAll() throws SQLException, ClassNotFoundException {
        return doctorDAO.findAll();
    }

    public void bookTimeSlot(Doctor doctor, TimeSlot timeSlot, User user, LocalDate date) throws SQLException, ClassNotFoundException {
        Doctor doctor1 = doctorDAO.find(doctor.getUuid());
        if (doctor1 == null)
            throw new NoSuchDoctorException();
        doctor1.getWorkList().stream().filter(e -> e.getDate() == date).findFirst().ifPresent(work -> work.bookTimeSlot(timeSlot, user));
    }

    public TimeSlot getNearestTimeSlot(Doctor doctor, TimeSlot timeSlot, LocalDate date) throws NoSuchTimeSlotAvailableException, SQLException, ClassNotFoundException {
        Doctor doctor1 = doctorDAO.find(doctor.getUuid());
        if (doctor1 == null)
            throw new NoSuchDoctorException();
        Work work = doctor1.getWorkList().stream().filter(e -> e.getDate() == date).findFirst().orElse(null);

        try {
            TimeSlot nearestTimeSlot = null;
            if (work != null) {
                nearestTimeSlot = work.getNearestTimeSlot(timeSlot);
                logger.info("got closer time slot " + nearestTimeSlot);
            }
            return nearestTimeSlot;
        } catch (NoSuchTimeSlotAvailableException e) {
            logger.error("not available time slots", e);
            throw e;
        }
    }

    public boolean isTimeSlotAvailable(Doctor doctor, TimeSlot timeSlot, LocalDate date) throws SQLException, ClassNotFoundException {
        logger.info("is time slot available " + doctor.toString() + " " + timeSlot.toString());
        Doctor doctor1 = doctorDAO.find(doctor.getUuid());
        if (doctor1 == null)
            throw new NoSuchDoctorException();
        Work work = doctor1.getWorkList().stream().filter(e -> e.getDate() == date).findFirst().orElse(null);
        boolean timeSlotAvailable = false;
        if (work != null) {
            timeSlotAvailable = work.isTimeSlotAvailable(timeSlot);
            logger.info("is time slot available result " + timeSlotAvailable);
        }
        return timeSlotAvailable;
    }

    public Doctor findByUser(UUID uuid) throws SQLException, ClassNotFoundException {
        return doctorDAO.findByUser(uuid);
    }

    public Doctor findByUser(User user) throws SQLException, ClassNotFoundException {
        return findByUser(user.getUuid());
    }

    public List<TimeSlot> getAvailableTimeSlots(Doctor doctor, LocalDate date) throws SQLException, ClassNotFoundException {
        Doctor doctor1 = doctorDAO.find(doctor.getUuid());
        Work work = doctor1.getWorkList().stream().filter(e -> e.getDate().equals(date)).filter(Work::isActive).findFirst().orElse(null);

        if (work != null) {
            return work.getAvailableTimeSlots();
        }
        return new ArrayList<>();
    }

    public void addWorkDay(Work work, Map<String, String> errors) throws SQLException, ClassNotFoundException {
        Map<String, String> valid = work.isValid();
        valid.forEach(errors::putIfAbsent);

        LocalDate finalDate = work.getDate();
        Doctor doctor = work.getDoctor();
        workDAO.findByDoctor(doctor.getUuid()).stream().filter(e -> e.getDate().equals(finalDate)).filter(Work::isActive).findFirst().ifPresent(first -> errors.put("date", "Doctor is already work in this day"));
        if (errors.isEmpty()) {
            workDAO.save(work);
        }
    }

    public void addDoctor(String profession, UUID userUUID) throws SQLException, ClassNotFoundException {
        User user = userDao.find(userUUID);
        Doctor doctor = doctorDAO.findByUser(userUUID);
        if (doctor != null) {
            doctor.setProfession(profession)
                    .setActive(true);
        } else {
            doctor = new Doctor()
                    .setUser(new User().setUuid(userUUID))
                    .setProfession(profession).setActive(true);
        }
        save(doctor);
        user.getRoles().add(Roles.doctor);
        roleService.saveRolesToUser(user);
    }

    public void deleteDoctor(UUID id) throws SQLException, ClassNotFoundException {
        Doctor doctor = find(id);
        User user = userDao.find(doctor.getUser().getUuid());
        user.getRoles().remove(Roles.doctor);
        roleService.saveRolesToUser(user);
        doctor.setActive(false);
        List<Visit> wait = new ArrayList<>();
        for (Work e : doctor.getWorkList()) {
            wait.addAll(e.getVisits().stream().filter(t -> t.getStatus().equals("wait")).collect(Collectors.toList()));
        }
        wait.forEach(e -> {
            try {
                e.setStatus("cancel");
                visitDAO.save(e);
            } catch (SQLException | ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        });
        doctor.getWorkList().forEach(e -> e.setActive(false));
        doctor.getWorkList().forEach(e -> {
            try {
                workDAO.save(e);
            } catch (SQLException | ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        });
        save(doctor);
    }

    public void bookSomeTimeSlot(TimeSlot timeSlot, Map<String, String> errors, UUID doctorUUID, LocalDate date, boolean nearest, String url, User user) throws SQLException, ClassNotFoundException, NoSuchTimeSlotAvailableException {
        Doctor doctor = find(doctorUUID);
        Work work = doctor.getWorkList().stream().filter(e -> e.getDate().equals(date)).filter(Work::isActive).findFirst().orElse(null);
        if (errors.isEmpty()) {
            if (work != null) {
                if (nearest) {
                    timeSlot = work.getNearestTimeSlotToUser(timeSlot,user);
                    timeSlotDAO.save(timeSlot);
                    TimeSlot finalTimeSlot = timeSlot;
                    TimeSlot slot = timeSlotDAO.findFreeTimeSlots().stream().filter(e -> e.getStartTime().equals(finalTimeSlot.getStartTime()) && e.getDuration() == finalTimeSlot.getDuration()).findFirst().orElseThrow(() -> new NoSuchTimeSlotAvailableException("All time slots are busy"));
                    Visit visit;
                    visit = new Visit().setUser(user).setTimeSlot(slot).setWork(work);
                    Map<String, String> valid = visit.isValid();
                    if (valid.isEmpty())
                        visitDAO.save(visit);
                    valid.forEach(errors::putIfAbsent);
                } else {
                    if (work.isTimeSlotAvailable(timeSlot)) {
                        timeSlotDAO.save(timeSlot);
                        TimeSlot finalTimeSlot1 = timeSlot;
                        TimeSlot slot = timeSlotDAO.findFreeTimeSlots().stream().filter(e -> e.getStartTime().equals(finalTimeSlot1.getStartTime()) && e.getDuration() == finalTimeSlot1.getDuration()).findFirst().orElseThrow(() -> new NoSuchTimeSlotAvailableException("Time slot is busy"));
                        Visit visit;
                        visit = new Visit().setUser(user).setUser(user).setTimeSlot(slot).setWork(work);
                        Map<String, String> valid = visit.isValid();
                        if (valid.isEmpty())
                            visitDAO.save(visit);
                        valid.forEach(errors::putIfAbsent);
                    } else {
                        throw new NoSuchTimeSlotAvailableException("Time slot is busy");
                    }
                }
            } else {
                throw new NoSuchTimeSlotAvailableException("The doctor does not work on that day");
            }
        }
    }
}
