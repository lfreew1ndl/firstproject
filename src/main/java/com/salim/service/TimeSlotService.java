package com.salim.service;

import com.salim.model.TimeSlot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.salim.dao.TimeSlotDAO;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class TimeSlotService {
    private final Logger logger = LogManager.getLogger(DoctorService.class);
    private TimeSlotDAO timeSlotDAO = new TimeSlotDAO();

    public TimeSlot find(UUID uuid) throws SQLException, ClassNotFoundException {
        return timeSlotDAO.find(uuid);
    }

    public TimeSlot find(TimeSlot timeSlot) throws SQLException, ClassNotFoundException {
        return find(timeSlot.getUuid());
    }

    public List<TimeSlot> findFreeTimeSlots() throws SQLException, ClassNotFoundException {
        return timeSlotDAO.findFreeTimeSlots();
    }
    public void save(TimeSlot timeSlot) throws SQLException, ClassNotFoundException {
        timeSlotDAO.save(timeSlot);
    }

    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        timeSlotDAO.deleteById(uuid);
    }

    public void delete(TimeSlot timeSlot) throws SQLException, ClassNotFoundException {
        deleteById(timeSlot.getUuid());
    }

    public List<TimeSlot> findAll() throws SQLException, ClassNotFoundException {
        return timeSlotDAO.findAll();
    }
}
