package com.salim.service;

import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.model.Visit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.salim.dao.VisitDAO;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class VisitService {
    private final Logger logger = LogManager.getLogger(DoctorService.class);
    private VisitDAO visitDAO = new VisitDAO();

    public Visit find(UUID uuid) throws SQLException, ClassNotFoundException {
        return visitDAO.find(uuid);
    }

    public Visit find(Visit visit) throws SQLException, ClassNotFoundException {
        return find(visit.getUuid());
    }

    public List<Visit> findByUser(UUID uuid) throws SQLException, ClassNotFoundException {
        return visitDAO.findByUser(uuid);
    }

    public List<Visit> findByUser(User user) throws SQLException, ClassNotFoundException {
        return findByUser(user.getUuid());
    }

    public List<Visit> findByDoctor(Doctor doctor) throws SQLException, ClassNotFoundException {
        return findByDoctor(doctor.getUuid());
    }
    public List<Visit> findByDoctor(UUID uuid) throws SQLException, ClassNotFoundException {
        return visitDAO.findByDoctor(uuid);
    }

    public void save(Visit visit) throws SQLException, ClassNotFoundException {
        visitDAO.save(visit);
    }

    public void deleteById(UUID uuid) throws SQLException, ClassNotFoundException {
        visitDAO.deleteById(uuid);
    }

    public void delete(Visit visit) throws SQLException, ClassNotFoundException {
        deleteById(visit.getUuid());
    }

    public List<Visit> findAll(String status) throws SQLException, ClassNotFoundException {
        return visitDAO.findAll(status);
    }

    public List<Visit> findAll() throws SQLException, ClassNotFoundException {
        return visitDAO.findAll();
    }
}
