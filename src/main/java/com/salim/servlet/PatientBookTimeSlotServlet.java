package com.salim.servlet;

import com.salim.model.*;
import com.salim.model.exception.NoSuchTimeSlotAvailableException;
import com.salim.service.DoctorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@WebServlet(name = "PatientBookTimeSlot", urlPatterns = "/patient/bookTimeSlot")
public class PatientBookTimeSlotServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    private final Logger logger = LogManager.getLogger(PatientBookTimeSlotServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            List<Doctor> all = doctorService.findAll();
            request.setAttribute("doctors", all);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened Patient boot time slot page");

        request.getRequestDispatcher("/jsp/patient/patientBookTimeSlot.jsp").forward(request, response);
    }

    @Override
    protected synchronized void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Try to book time slot");
        Map<String, String> errors = new HashMap<>();
        UUID doctorUUID = UUID.fromString(req.getParameter("doctorUUID"));
        LocalDate date = null;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
            date = LocalDate.parse(req.getParameter("date"),formatter);
        } catch (DateTimeException e) {
            logger.error(e);
            errors.put("date","Date must be correct");
        }
        LocalTime startTime = null;
        try {
            String time = req.getParameter("startTime");
            String s = time.replaceAll(" ", "");
            startTime = LocalTime.parse(s);
        } catch (DateTimeException e) {
            logger.error(e);
            errors.put("startTime","Start time must be correct");
        }
        Integer duration = Integer.parseInt(req.getParameter("duration"));
        String near = req.getParameter("nearest");
        String url = "/patient";
        boolean nearest = near != null && near.equals("on");
        TimeSlot timeSlot = new TimeSlot().setStartTime(startTime).setDuration(duration);
        timeSlot.isValid().forEach(errors::putIfAbsent);

        try {
            User user = (User) req.getSession().getAttribute("user");
            doctorService.bookSomeTimeSlot(timeSlot,errors,doctorUUID,date,nearest,url,user);
            if (!errors.isEmpty()) {
                try {
                    List<Doctor> all = doctorService.findAll();
                    req.setAttribute("doctors", all);

                } catch (SQLException | ClassNotFoundException e) {
                    logger.error(e);
                    e.printStackTrace();
                }
                req.setAttribute("errors", errors);
                req.getRequestDispatcher("/jsp/patient/patientBookTimeSlot.jsp").forward(req, resp);
            }else {
                logger.info("Successfully booked time slot");
                url+="?info=Successfully added.";
            }
        } catch (SQLException | ClassNotFoundException | NoSuchTimeSlotAvailableException e) {
            logger.error(e);
            url += "?error=" + e.getMessage();
        }

        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() +url));
    }
}
