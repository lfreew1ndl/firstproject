package com.salim.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AboutPage", urlPatterns = "/about")
public class AboutServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(AboutServlet.class);


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Opened About Page");

        request.getRequestDispatcher("/jsp/about.jsp").forward(request, response);
    }
}
