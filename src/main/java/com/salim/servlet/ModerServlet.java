package com.salim.servlet;

import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "ModerPage", urlPatterns = "/staff")
public class ModerServlet extends HttpServlet {

    private DoctorService doctorService = new DoctorService();
    private UserService userService = new UserService();
    private final Logger logger = LogManager.getLogger(ModerServlet.class);


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            List<Doctor> doctors = doctorService.findAll();
            request.setAttribute("doctors",doctors);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened moder page");

        request.getRequestDispatcher("/jsp/moder/moder.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UUID id = UUID.fromString(req.getParameter("record_id"));
        logger.info("Try to remove doctor");
        try {
            doctorService.deleteDoctor(id);
            logger.info("Successfully deleted doctor.");
            User user = userService.find((User) req.getSession().getAttribute("user"));
            if (user != null)
                req.getSession().setAttribute("user",user);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() +"/staff?info=Successfully deleted"));
    }
}