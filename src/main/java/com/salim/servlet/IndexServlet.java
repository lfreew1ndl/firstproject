package com.salim.servlet;

import com.salim.model.enums.Roles;
import com.salim.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "IndexServlet", urlPatterns = {"/index"})
public class IndexServlet extends HttpServlet {
    private UserService userService = new UserService();
    private final Logger logger = LogManager.getLogger(IndexServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int doctors = userService.countOfUsers(Roles.doctor);
            int patients = userService.countOfUsers(Roles.user);
            req.setAttribute("doctors",doctors);
            req.setAttribute("patients",patients);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened index page");
        req.getRequestDispatcher("/jsp/index.jsp").forward(req, resp);
    }
}
