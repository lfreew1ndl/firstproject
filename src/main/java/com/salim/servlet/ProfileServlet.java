package com.salim.servlet;

import com.salim.model.User;
import com.salim.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ProfileServlet", urlPatterns = "/profile")
public class ProfileServlet extends HttpServlet {

    private UserService userService = new UserService();
    private final Logger logger = LogManager.getLogger(ProfileServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Opened profile page");
        req.getRequestDispatcher("/jsp/profile.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Try to change profile");

        Map<String, String> errors = new HashMap<>();

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        LocalDate birthday = null;
        User user = null;
        try {
            user = userService.find(((User) req.getSession().getAttribute("user")).getUuid());
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
        if (user != null) {
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
                if (!req.getParameter("date").equals(user.getBirthday().toString())) {
                    birthday = LocalDate.parse(req.getParameter("date"), formatter);
                    user.setBirthday(birthday);
                }
            } catch (DateTimeParseException e) {
                logger.error(e);
                errors.put("birthday", "Date must be correct!");
            }

            user.setEmail(email).setEmail(email).setLastName(lastName).setFirstName(firstName);
            Map<String, String> valid = user.isValid();
            valid.remove("login");
            valid.forEach(errors::putIfAbsent);
            if (errors.isEmpty()) {
                try {
                    logger.info("Successfully changed profile ");
                    userService.save(user);
                    req.getSession().setAttribute("user", user);
                    resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/profile?info=Successfully edited."));
                } catch (SQLException | ClassNotFoundException e) {
                    logger.error(e);
                    e.printStackTrace();
                }
            } else {
                req.setAttribute("errors", errors);
                req.getRequestDispatcher("/jsp/profile.jsp").forward(req, resp);
            }
        }
    }
}
