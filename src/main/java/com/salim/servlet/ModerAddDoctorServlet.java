package com.salim.servlet;

import com.salim.model.User;
import com.salim.service.DoctorService;
import com.salim.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "ModerAddDoctorPage", urlPatterns = "/staff/addDoctor")
public class ModerAddDoctorServlet extends HttpServlet {

    private UserService userService = new UserService();
    private DoctorService doctorService = new DoctorService();
    private final Logger logger = LogManager.getLogger(ModerAddDoctorServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            List<User> all = userService.findFreeUsers();
            request.setAttribute("users",all);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened Moder add doctor page");

        request.getRequestDispatcher("/jsp/moder/moderAddDoctor.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Try to add doctor");

        UUID userUUID = UUID.fromString(req.getParameter("userUUID"));
        String profession = req.getParameter("profession");
        String url = "/staff";
        try {
            doctorService.addDoctor(profession,userUUID);
            url+="?info=Successfully added doctor.";
            logger.info("Successfully added doctor");
            User user = userService.find((User) req.getSession().getAttribute("user"));
            if (user != null)
                req.getSession().setAttribute("user",user);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            url+="?error= "+"Unknown error";
        }
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() +url));
    }
}