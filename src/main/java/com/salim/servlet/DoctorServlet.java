package com.salim.servlet;

import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.model.Visit;
import com.salim.service.DoctorService;
import com.salim.service.VisitService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "DoctorPage", urlPatterns = "/doctor")
public class DoctorServlet extends HttpServlet {

    private DoctorService doctorService = new DoctorService();
    private VisitService visitService = new VisitService();
    private final Logger logger = LogManager.getLogger(DoctorServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            Doctor doctor = doctorService.findByUser((User) request.getSession().getAttribute("user"));
            List<Visit> byDoctor = visitService.findByDoctor(doctor);
            byDoctor.sort((o1, o2) -> {
                if (o1.getWork().getDate().equals(o2.getWork().getDate()))
                    return o1.getStartTime().compareTo(o2.getStartTime());
                return o1.getWork().getDate().compareTo(o2.getWork().getDate());
            });

            request.setAttribute("doctor",doctor);
            request.setAttribute("visits",byDoctor);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened Doctor page");
        request.getRequestDispatcher("/jsp/doctor/doctor.jsp").forward(request, response);
    }

    @Override
    @SuppressWarnings("Duplicates")
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Try to cancel visit.");
        UUID id = UUID.fromString(req.getParameter("record_id"));
        try {
            Visit visit = visitService.find(id);
            visit.setStatus("cancel");
            visitService.save(visit);
            logger.info("Successfully canceled visit by doctor");
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
        resp.sendRedirect("/doctor?info=Successfully deleted");
    }
}