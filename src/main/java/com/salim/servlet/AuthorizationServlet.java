package com.salim.servlet;

import com.salim.model.User;
import com.salim.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "LoginPage", urlPatterns = "/login")
public class AuthorizationServlet extends HttpServlet {

    private UserService userService = new UserService();
    private final Logger logger = LogManager.getLogger(AuthorizationServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Opened Authorization Page");
        request.getRequestDispatcher("/jsp/authorization.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Try to log in");
        String login = req.getParameter("login");
        login = login.toLowerCase();
        String password = req.getParameter("password");
        try {
            User user = new User().setLogin(login).setPassword(password);
            user = userService.login(user);
            req.setAttribute("user", user);
            if (user != null) {
                HttpSession session = req.getSession(true);
                session.setAttribute("user", user);
                session.setMaxInactiveInterval(300000);
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/"));
                logger.info("Loggined " + user.getLogin());
            } else {
                logger.info("Wrong password or login");
                req.setAttribute("errorMessage", "Login failed: Invalid username or password.");
                req.getRequestDispatcher("/jsp/authorization.jsp").forward(req, resp);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }

    }
}
