package com.salim.servlet;

import com.salim.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "IsUsernameAvailableServlet",urlPatterns = "/isUsernameAvailable")
public class IsUsernameAvailableServlet extends HttpServlet {

    private UserService userService = new UserService();
    private final Logger logger = LogManager.getLogger(IsUsernameAvailableServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameterValues("login")[0];
        logger.info("Checked is login free (login =" + login+")");

        try {
            String str = userService.isLoginAvailable(login) ? "true" : "false";
            PrintWriter out = resp.getWriter();
            out.print(str);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
    }
}
