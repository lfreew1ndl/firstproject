package com.salim.servlet;

import com.salim.model.*;
import com.salim.service.DoctorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@WebServlet(name = "PatientGetAvailableSlotsPage", urlPatterns = "/patient/getAvailableSlots")
public class PatientGetAvailableSlotsServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    private final Logger logger = LogManager.getLogger(PatientGetAvailableSlotsServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            List<Doctor> all = doctorService.findAll();
            request.setAttribute("doctors",all);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened patient get available slots page");
        request.getRequestDispatcher("/jsp/patient/patientGetAvailableSlots.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> errors = new HashMap<>();
        logger.info("Try to get available time slots");
        try {
            List<Doctor> all = doctorService.findAll();
            req.setAttribute("doctors",all);
            String doctorUUID = req.getParameter("doctorUUID");
            req.setAttribute("doctorUUID1",doctorUUID);
            LocalDate date = null;
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
                date = LocalDate.parse(req.getParameter("date"),formatter);
                req.setAttribute("date1",req.getParameter("date"));

            } catch (DateTimeException e) {
                logger.error(e);
                errors.put("date","Date must be correct");
            }
            Doctor doctor = doctorService.find(UUID.fromString(doctorUUID));
            if (doctor == null){
                errors.put("doctor","Doctor must exist!");
            }
            List<TimeSlot> availableTimeSlots = doctorService.getAvailableTimeSlots(doctor,date);
            availableTimeSlots.sort(Comparator.comparing(TimeSlot::getStartTime));
            logger.info("Getted available slots");
            req.setAttribute("timeSlots",availableTimeSlots);
            req.setAttribute("errors",errors);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }

        req.getRequestDispatcher("/jsp/patient/patientGetAvailableSlots.jsp").forward(req, resp);
    }
}
