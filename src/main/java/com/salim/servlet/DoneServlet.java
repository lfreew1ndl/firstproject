package com.salim.servlet;

import com.salim.model.Visit;
import com.salim.service.VisitService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet(name = "DoneServlet", urlPatterns = "/recordDone")
public class DoneServlet extends HttpServlet {

    private VisitService visitService = new VisitService();
    private final Logger logger = LogManager.getLogger(DoneServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Try set visit successful");
        UUID id = UUID.fromString(req.getParameter("record_id"));
        try {
            Visit visit = visitService.find(id);
            visit.setStatus("successful");
            visitService.save(visit);
            logger.info("Successfully setted successful for visit");
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() +"/doctor?info=Successfully updated"));
    }
}
