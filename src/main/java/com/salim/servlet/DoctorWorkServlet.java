package com.salim.servlet;

import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.model.Work;
import com.salim.service.DoctorService;
import com.salim.service.VisitService;
import com.salim.service.WorkService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@WebServlet(name = "DoctorWorkPage", urlPatterns = "/doctor/work")
public class DoctorWorkServlet extends HttpServlet {

    private WorkService workService = new WorkService();
    private DoctorService doctorService = new DoctorService();
    private VisitService visitService = new VisitService();
    private final Logger logger = LogManager.getLogger(DoctorWorkServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            Doctor doctor = doctorService.findByUser(user.getUuid());
            List<Work> works = workService.findByDoctor(doctor).stream().filter(Work::isActive).sorted(Comparator.comparing(Work::getDate)).collect(Collectors.toList());
            request.setAttribute("works",works);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        logger.info("Opened Doctor Work page");
        request.getRequestDispatcher("/jsp/doctor/doctorWork.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Try to cancel work day");
        UUID id = UUID.fromString(req.getParameter("record_id"));
        try {
            Work work = workService.find(id);
            work.getVisits().stream().filter(e-> e.getStatus().equals("wait")).forEach(e->e.setStatus("cancel"));
            work.getVisits().forEach(e->{
                try {
                    visitService.save(e);
                } catch (SQLException | ClassNotFoundException e1) {
                    logger.error(e1);
                    e1.printStackTrace();
                }
            });
            work.setActive(false);
            logger.info("Successfully canceled work day");
            workService.save(work);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() +"/doctor/work?info=Successfully deleted"));
    }
}