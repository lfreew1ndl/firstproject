package com.salim.servlet;

import com.salim.model.Doctor;
import com.salim.model.User;
import com.salim.model.Work;
import com.salim.service.DoctorService;
import com.salim.service.WorkService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "DoctorAddWorkDayPage", urlPatterns = "/doctor/addWorkDay")
public class DoctorAddWorkDayServlet extends HttpServlet {
    private WorkService workService = new WorkService();
    private DoctorService doctorService = new DoctorService();
    private final Logger logger = LogManager.getLogger(DoctorAddWorkDayServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Opened DoctorAddWorkDay Page ");
        request.getRequestDispatcher("/jsp/doctor/doctorAddWorkDay.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Try to add work day");
        Map<String, String> errors = new HashMap<>();
        User user = (User) req.getSession().getAttribute("user");
        try {
            Doctor doctor = doctorService.findByUser(user);
            String startTime1 = req.getParameter("startTime");
            String endTime1 = req.getParameter("endTime");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH : mm");
            LocalTime startTime = null;
            try {
                startTime = LocalTime.parse(startTime1, dtf);
            } catch (DateTimeParseException e) {
                errors.put("start time", "Time must be correct!");
            }
            LocalTime endTime = null;
            try {
                endTime = LocalTime.parse(endTime1, dtf);
            } catch (DateTimeParseException e) {
                errors.put("end time", "Time must be correct!");
            }
            LocalDate date = null;
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
                date = LocalDate.parse(req.getParameter("date"), formatter);
            } catch (DateTimeParseException e) {
                errors.put("date", "Date must be correct!");
            }

            Work work = new Work().setDate(date).setStartTime(startTime).setEndTime(endTime).setDoctor_id(doctor.getUuid()).setDoctor(doctor);
            doctorService.addWorkDay(work, errors);

            if (errors.isEmpty()) {
                logger.info("Work day added");
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/doctor/work?info=Successfully added work day."));
            } else {
                logger.info("Work day not added");
                req.setAttribute("errors", errors);
                req.getRequestDispatcher("/jsp//doctor/doctorAddWorkDay.jsp").forward(req, resp);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            e.printStackTrace();
        }
    }
}