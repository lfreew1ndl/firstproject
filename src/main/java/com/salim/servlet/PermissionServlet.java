package com.salim.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PermissionServlet", urlPatterns = "/permissionURI")
public class PermissionServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(PermissionServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Opened permission page");
        req.getRequestDispatcher("/jsp/permission.jsp").forward(req, resp);
    }
}
