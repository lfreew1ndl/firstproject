package com.salim.servlet;

import com.salim.model.User;
import com.salim.model.enums.Roles;
import com.salim.service.RoleService;
import com.salim.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "RegistrationPage", urlPatterns = "/register")
public class RegistrationServlet extends HttpServlet {

    private UserService userService = new UserService();
    private RoleService roleService = new RoleService();
    private final Logger logger = LogManager.getLogger(RegistrationServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Opened register page");


        request.getRequestDispatcher("/jsp/registration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Try to register");

        try {
            Map<String,String> errors = new HashMap<>();
            String login = req.getParameter("login");
            login = login.toLowerCase();
            String firstName = req.getParameter("firstName");
            String lastName = req.getParameter("lastName");
            String email = req.getParameter("email");
            String date = req.getParameter("date");
            String password = req.getParameter("password");
            LocalDate birthday;
            User user = new User();
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
                birthday = LocalDate.parse(date, formatter);
                user.setBirthday(birthday);
            }catch (DateTimeParseException e){
                logger.error(e);
                errors.put("birthday","Date must be correct!");
            }
            user.setLogin(login).setFirstName(firstName).setLastName(lastName).setEmail(email).setPassword(password);
            Map<String, String> valid = user.isValid();
            valid.forEach(errors::putIfAbsent);

            if (errors.isEmpty()) {
                userService.save(user);
                User user1 = userService.login(user);
                user1.getRoles().add(Roles.user);
                roleService.saveRolesToUser(user1);
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() +"/"));
                HttpSession session = req.getSession(true);
                session.setAttribute("user", user1);
                session.setMaxInactiveInterval(300000);
                logger.info("Successfully registered");

            }else {
                req.setAttribute("errors",errors);
                req.getRequestDispatcher("/jsp/registration.jsp").forward(req, resp);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
            req.getRequestDispatcher("/jsp/registration.jsp").forward(req, resp);
            e.printStackTrace();
        }
    }
}