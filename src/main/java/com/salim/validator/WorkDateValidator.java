package com.salim.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class WorkDateValidator implements ConstraintValidator<WorkDate, LocalDate> {
    public void initialize(WorkDate constraintAnnotation) {
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        return value.isAfter(LocalDate.now());
    }
}