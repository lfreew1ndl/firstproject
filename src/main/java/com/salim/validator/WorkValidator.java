package com.salim.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WorkValidator implements ConstraintValidator<Work, com.salim.model.Work> {
    public void initialize(Work constraintAnnotation) {
    }

    @Override
    public boolean isValid(com.salim.model.Work value, ConstraintValidatorContext context) {
        return value.getStartTime().isBefore(value.getEndTime());
    }
}