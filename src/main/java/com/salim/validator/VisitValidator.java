package com.salim.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalTime;

public class VisitValidator implements ConstraintValidator<Visit, com.salim.model.Visit> {
    public void initialize(Visit constraintAnnotation) {
    }

    @Override
    public boolean isValid(com.salim.model.Visit value, ConstraintValidatorContext context) {
        if (value.getWork().getDate().isAfter(LocalDate.now()))
            return true;
        return  (value.getWork().getDate().equals(LocalDate.now()) && value.getTimeSlot().getStartTime().isAfter(LocalTime.now()));
    }
}