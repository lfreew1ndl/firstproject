package com.salim.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = { VisitValidator.class })
public @interface Visit {
    String message() default "Visit must be in future.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
