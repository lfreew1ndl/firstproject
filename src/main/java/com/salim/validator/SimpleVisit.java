package com.salim.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = { SimpleVisitValidator.class })
public @interface SimpleVisit {
    String message() default "You already have a visit at this time.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
