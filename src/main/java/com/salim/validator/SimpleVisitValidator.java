package com.salim.validator;


import com.salim.model.Visit;
import com.salim.service.UserService;
import com.salim.service.VisitService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class SimpleVisitValidator implements ConstraintValidator<SimpleVisit, com.salim.model.Visit> {
    private VisitService visitService = new VisitService();
    public void initialize(SimpleVisit constraintAnnotation) {
    }

    @Override
    public boolean isValid(com.salim.model.Visit value, ConstraintValidatorContext context) {
        try {
            List<Visit> byUser = visitService.findByUser(value.getUser());
            Visit visit = byUser.stream().filter(e -> e.getWork().getDate().equals(value.getWork().getDate())).filter(e -> value.getEndTime().isAfter(e.getStartTime()) && (value.getStartTime().isBefore(e.getEndTime()))).filter(e-> e.getStatus().equals("wait")).findAny().orElse(null);
            if (visit == null)
                return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
}