package com.salim.dataStorage;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class DataStorageJDBC {
    private Connection con = null;

    private static DataStorageJDBC dataStorageJDBC = null;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (dataStorageJDBC == null){
            dataStorageJDBC = new DataStorageJDBC();
        }
        return dataStorageJDBC.getCon();
    }

    private void init() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        String propFileName = "config.properties";

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        Properties prop = new Properties();

        try {
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String url = prop.getProperty("database.url");
        String login = prop.getProperty("database.user");
        String password = prop.getProperty("database.password");
        con = DriverManager.getConnection(url,login,password);
    }

    private Connection getCon() {
        return con;
    }

    private DataStorageJDBC() throws SQLException, ClassNotFoundException {
        init();
    }
}